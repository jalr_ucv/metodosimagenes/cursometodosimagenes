# Clase 8: Retroproyección filtrada +
## Introducción

* Objetivos:
    * La retroproyección filtrada en partes.
    * Transformada de Hilbert.
    * Tarea series de Fourier.

## Retroproyección filtrada en partes
* Tomemos el filtro y separemos en partes

$$\mathcal{GR}f(t,\vec\omega)=\frac{1}{2\pi}\int_{-\infty}^{\infty} e^{ir\mathbf{x}\cdot\vec{\omega}} \widetilde{\mathcal{Rf}}(r,\vec{\omega})|r|dr.$$

* Parte 1: multiplicar la función $\widetilde{\mathcal{Rf}}(r,\vec{\omega})$ por $r$.
    * Favorece la inclusión de frecuencias altas.
    * Está relacionado con una derivada en el espacio directo.
* Parte 2: multiplcar por la función signo de $r$

$$sgn(r)=\lbrace \begin{array}{cc} -1 & r<0 \\ 0 &  r=0 \\ 1 & r>0\end{array}\right.$$


## Transformada de Hilbert

* Definimos la transformada de Hilbert

$$\mathcal{H}f\equiv \mathcal{F}^{-1}(sgn\hat{f}).$$

* Contrario al caso del producto por $r$, que involucra una derivada, la transformada de Hilbert es una operacion no local en r.

$$\mathcal{H}f \sim \mathcal{F}^{-1}(sgn)\star {f}.$$

En caso que se pudiera definir la transformada $\mathcal{F}^{-1}(sgn)$.

* ¿Por qué no existe $\mathcal{F}^{-1}(sgn)$?

## Aproximación a la transformada de Hilbert como convolución

* Podemos buscar una secuencia de funciones integrables que convergen a la función $sgn$.

$$\phi_\epsilon(\xi)  =\hat{h}_\epsilon(\xi) \equiv sgn(\xi)~e^{-\epsilon|\xi|}.$$

* Se cumple

$$h_\epsilon(t) = \frac{i}{\pi}\frac{t}{t^2+\epsilon^2}.$$

* Se define $\mathcal{H}_\epsilon f$

$$\mathcal{H}_\epsilon f = \mathcal{F}^{-1}(\hat{h}_\epsilon\hat{f}) = f \star h_\epsilon.$$

## Inversa aproximada de la Transformación de Radon.

* Recordamos la relación entre la derivada y la convolución

$$\partial_x(f\star g) = \partial_x f \star g = f \star \partial_x g.$$

* Ahora pegamos los pedazos. La inversa aproximada de la transformada de Radon

$$f(\vec x) \approx \frac{1}{2\pi i}\int_0^{\pi} \mathcal{H}_\epsilon (\partial_t \mathcal{R}f)(\vec x \cdot \vec \omega,\vec\omega)$$

* Sustituyendo la transformación de Hilbert

$$f(\vec x) \approx \frac{1}{2\pi i}\int_0^{\pi} h_\epsilon \star (\partial_t \mathcal{R}f)(\vec x \cdot \vec \omega,\vec\omega)$$

$$f(\vec x) \approx \frac{1}{2\pi i}\int_0^{\pi} \partial_t h_\epsilon \star (\mathcal{R}f)(\vec x \cdot \vec \omega,\vec\omega)$$

## Ventajas

* Es una operación lineal simple

* Elimina problemas
    * Derivada sobre la transformada de Radon (ruido)
    * Convolución con transformada de la función $sgn$ (no existe)
* Discutir

## Tarea

* Repasar series de Fourier y hacer 7.1.3