# Clase 1: Medidas y modelado
## Reglas del juego
* El curso tiene 16 semanas
* Datos
    * José Antonio López Rodríguez
    * jal.ccs@gmail.com
* Fechas 2023-07-31 al 2023-11-13
* Evaluación
    * Tareas 25%
    * Examen 2023-09-25 (40%)
    * Proyecto final 2023-11-13 (35%)
## Resumen
### Introducción
* Un modelo cuantitativo de un sistema físico se expresa en el lenguaje de las matemáticas. Un modelo cualitativo a menudo precede a un modelo cuantitativo.
* Durante muchos años, los médicos utilizaron imágenes de rayos X médicas sin utilizar un modelo cuantitativo preciso. Los rayos X se consideraban como "luz" de alta frecuencia con tres propiedades muy útiles:
    * Si los rayos X inciden en un cuerpo humano, una fracción de la radiación incidente se absorbe o se dispersa, aunque una fracción considerable se transmite. La fracción absorbida o dispersada es proporcional a la densidad total del material encontrado. La disminución general de la intensidad del haz de rayos X se llama atenuación.
    * Un haz de luz de rayos X viaja en línea recta.
    * Los rayos X oscurecen la película fotográfica. La opacidad de la película es una función monótona de la energía incidente.
* Tomadas en conjunto, estas propiedades significan que, utilizando los rayos X, se puede "ver a través" de un cuerpo humano para obtener una sombra o proyección de la anatomía interna en una hoja de película.


* Las imágenes de rayos X revolucionaron la práctica de la medicina porque abrieron la puerta al examen no invasivo de la anatomía interna.
* Sin embargo, las imágenes de rayos X tienen limitaciones, como la incapacidad de deducir el orden espacial en la tercera dimensión.
* Otra limitación es que las películas fotográficas son poco sensibles a los rayos X, por lo que las diferencias en la intensidad del haz de rayos X producen pequeñas diferencias en la opacidad de la película.
* Esto significa que el contraste entre los diferentes tejidos blandos es pobre.
* Alan Cormack y Godfrey Hounsfield desarrollaron la tomografía computarizada (TC) o imagen de corte para superar estas limitaciones.
* La TC utiliza una cantidad de mediciones de diferentes proyecciones bidimensionales para inferir la estructura anatómica tridimensional.
* La TC requiere detectores más sensibles, como los cristales de centelleo, y computadoras potentes para procesar las mediciones.
* La matemática es el lenguaje en el que se expresa cualquier teoría cuantitativa o modelo.
* En este capítulo se consideran una variedad de ejemplos de sistemas físicos, procesos de medición y los modelos matemáticos utilizados para describirlos.
* Se define el concepto de grados de libertad y se relaciona con la dimensión de un espacio vectorial.
* El capítulo concluye analizando el problema de reconstruir una región en el plano a partir de las mediciones de las sombras que proyecta.
## Modelado Matemático

* El primer paso para dar una descripción matemática de un sistema es aislarlo del universo en el que se encuentra.
* Un modelo práctico incluye el sistema de interés y las principales influencias sobre él. Los pequeños efectos se ignoran, aunque pueden volver, como error de medida y ruido, para acosar al modelo.
* Después de que el sistema está aislado, necesitamos encontrar una colección de parámetros numéricos que describan su estado. En esta generalidad, estos parámetros se denominan variables de estado.
* En el mundo idealizado de un sistema aislado, la medición exacta de las variables de estado determina de forma única el estado del sistema.
* Es posible que los parámetros que dan una descripción conveniente del sistema no sean directamente medibles. El modelo matemático describe entonces las relaciones entre las variables de estado.
* Utilizando estas relaciones, el estado del sistema a menudo puede determinarse a partir de mediciones factibles.
* Un ejemplo simple ayudará a aclarar estos conceptos abstractos.

* Ejemplo: Sistema simple de un "pelota en un círculo".
    * El estado del sistema se describe por las coordenadas del pelota (x, y).
    * Estas coordenadas están sujetas a la restricción $x^2 + y^2 = r^2$.
    * Si la pelota se acerca a la x-axis, las criaturas unidimensionales que viven en la x-axis pueden observar su sombra.
    * La sombra determina la x-coordenada de la pelota, y usando la ecuación ($x^2 + y^2 = r^2$) se puede obtener la y-coordenada: $x=\pm\sqrt{r^2-y^2}$.
    * Sin embargo, la signatura de la y-coordenada no se puede determinar con la información disponible.
    * Si la x-axis está iluminada con luz roja desde arriba y luz azul desde abajo, entonces las criaturas unidimensionales pueden determinar la signatura de la y-coordenada.
    * Con esta información adicional, la ubicación de la pelota se puede determinar completamente.
* El conjunto de todos los pares ordenados de números reales que satisfacen la restricción $x^2 + y^2 = r^2$ es el espacio de estado del sistema.
* En este caso, el espacio de estado es un círculo de radio r centrado en (0, 0).
#### Ejemplo para discutir
Supongamos que en el ejemplo anterior la pelota está atada a (0, 0) por una cuerda de longitud r. ¿Qué relaciones satisfacen las variables de estado (x, y)? ¿Hay una medición que los seres lineales puedan hacer para determinar la ubicación de la pelota? ¿Cuál es el espacio de estado para este sistema?
### Sistemas de finitos grados de libertad
* Los sistemas físicos se pueden modelar matemáticamente como conjuntos de ecuaciones y desigualdades.
* El estado de un sistema se puede describir mediante un conjunto de variables de estado.
    * El conjunto de variables de estado puede ser finito, como un $R^n$, o infinito.
    * Finitos grados de libertad o infinitos grados de libertad. 
    * Recordar espacios de Hilbert.
* Las ecuaciones y desigualdades que describen el sistema se denominan modelo matemático del sistema.
* Espacio de estado
    * El espacio de estado es el conjunto de todos los puntos que representan estados posibles del sistema.
    * Recordar el concepto de ligadura.
* Linealidad
    * Los sistemas lineales tienen un modelo matemático que consiste en un conjunto de ecuaciones lineales.
    * Los sistemas no lineales pueden aproximarse por sistemas lineales mediante el uso de la aproximación de Taylor.
* En los sistemas físicos a menudo se identifican variables de entrada y variables de salida, definidas a partir de los grados de libertad.
    * Las variables de entrada son las variables que se controlan, y las variables de salida son las variables que se miden.
* Problema inverso
    * El modelo matemático del sistema se puede usar para determinar las variables de estado a partir de las variables de salida.
#### Ejemplo de juguete
* Determinación de la altura de una montaña, usando la distancia a la base y el ángulo subtendido hasta la cumbre desde un punto de observación $P$.
    * ¿Cuáles son las variables de estado? ($d$,$h$,$\theta$,$l$)
    * ¿Cuáles son las ecuaciones que las relacionan? (trigonomtría)
    * ¿Cómo cuantificar el efecto de los errores de medición en las predicciones del modelo? (propagación de errores)
    * Modificación para evitar el problema del desconocimiento de la distancia del punto de observación a la base.

```latex {cmd=true hide=true}
\documentclass{standalone}
\usepackage{tikz}
\usetikzlibrary{matrix}
\begin{document}
\centering
\begin{tikzpicture}

\draw (0,0) -- (10,0);
\draw[green] (6,0) -- (8,4) -- (10,0);
\draw[blue,dashed] (0,0) -- (8,4);
\draw[<->,red,dashed] (0,-.2) -- (8,-.2);
\draw[<->,red,dashed] (8,0) -- (8,4);

\node[anchor=north] at (4,-.3) {$d$};
\node[anchor=north] at (8.5,2) {$h$?};
\node[anchor=east] at (1.1,0.22) {$\theta$};
\node[anchor=east] at (0,0.26) {$P$};

\end{tikzpicture}
\end{document}
```
#### Algunos problemas del modelo:
* **La forma de la montaña puede no ser como se ve en la figura**. Es imposible tener una linea de visión con la cima de la montaña. En este caso, el modelo matemático no será capaz de calcular la altura real del monte. Por lo tanto, es importante tener en cuenta la forma real del monte al crear el modelo matemático.
* **La curvatura de la Tierra no se tiene en cuenta en el modelo matemático**. Esto puede causar errores en el cálculo de la altura del monte si las distancias involucradas son grandes. Para evitar estos errores, es necesario utilizar un modelo matemático más sofisticado que tenga en cuenta la curvatura de la Tierra.
* **La geometría de las mediciones puede ser diferente de la geometría euclidiana utilizada en el modelo matemático**. Esto se debe a que la luz que viaja desde el monte al transitador puede ser refractada por la atmósfera. Para evitar este error, es necesario utilizar un modelo matemático que tenga en cuenta la refracción de la luz.

### Tarea
Ejercicios 1.1.3, 1.1.7, 1.1.8, 1.1.9, 1.1.13 y 1.1.14

