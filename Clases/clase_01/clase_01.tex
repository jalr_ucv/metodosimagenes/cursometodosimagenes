\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 1: Introducción + Medidas y modelado}

\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{31 de julio de 2023} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Reglas del juego}

\begin{itemize}
\item El curso tiene 16 semanas.
\item El instructor es José Antonio López Rodríguez.
\item El correo electrónico del instructor es jal.ccs@gmail.com.
\item Las fechas del curso son 2023-07-31 al \st{2023-11-13} 2023-11-16.
\item La evaluación del curso es la siguiente:
    \begin{itemize}
    \item Tareas: 25\%
    \item Examen el \st{2023-09-25} 2023-09-28: 40\%
    \item Proyecto final: 35\%
    \end{itemize}
\item Bibliografia: Epstein, Charles L., ed. \textit{Introduction to the mathematics of medical imaging}. Society for Industrial and Applied Mathematics, 2007.
\end{itemize}
\end{frame}

\begin{frame}{Introducción}
\begin{itemize}
  \item Un modelo cuantitativo de un sistema físico se expresa en el lenguaje de las matemáticas. Un modelo cualitativo a menudo precede a un modelo cuantitativo.
  \item Durante muchos años, los médicos utilizaron imágenes de rayos X médicas sin utilizar un modelo cuantitativo preciso. Los rayos X se consideraban como "luz" de alta frecuencia con tres propiedades muy útiles:
  \begin{itemize}
    \item Si los rayos X inciden en un cuerpo humano, una fracción de la radiación incidente se absorbe o se dispersa, aunque una fracción considerable se transmite. La fracción absorbida o dispersada es proporcional a la densidad total del material encontrado. La disminución general de la intensidad del haz de rayos X se llama atenuación.
    \item Un haz de luz de rayos X viaja en línea recta.
    \item Los rayos X oscurecen la película fotográfica. La opacidad de la película es una función monótona de la energía incidente.
  \end{itemize}
  %\item Tomadas en conjunto, estas propiedades significan que, utilizando los rayos X, se puede "ver a través" de un cuerpo humano para obtener una sombra o proyección de la anatomía interna en una hoja de película.
\end{itemize}
\end{frame}

\begin{frame}{Imágenes de rayos X y sus limitaciones}
\begin{itemize}
  \item Las imágenes de rayos X revolucionaron la práctica de la medicina porque abrieron la puerta al examen no invasivo de la anatomía interna.
  \item Limitaciones:
  \begin{itemize}
      \item Incapacidad de deducir el orden espacial en la tercera dimensión.
    \item Películas fotográficas son poco sensibles a los rayos X, por lo que las diferencias en la intensidad del haz de rayos X producen pequeñas diferencias en la opacidad de la película.
    \item El contraste entre los diferentes tejidos blandos es pobre.
  
  \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Tomografía Computarizada}
\begin{itemize}
  \item Alan Cormack y Godfrey Hounsfield desarrollaron la tomografía computarizada (TC) o imagen de corte para superar estas limitaciones.
  \item La TC utiliza una cantidad de mediciones de diferentes proyecciones bidimensionales para inferir la estructura anatómica tridimensional.
  \item La TC requiere detectores más sensibles, como los cristales de centelleo, y computadoras potentes para procesar las mediciones.
\end{itemize}
\end{frame}

\begin{frame}{Modelado Matemático}
\begin{itemize}
  \item El primer paso para dar una descripción matemática de un sistema es aislarlo del universo en el que se encuentra.
  \item Un modelo práctico incluye el sistema de interés y las principales influencias sobre él. Los pequeños efectos se ignoran, aunque pueden volver, como error de medida y ruido, para acosar al modelo.
  \item Después de que el sistema está aislado, necesitamos encontrar una colección de parámetros numéricos que describan su estado. En esta generalidad, estos parámetros se denominan variables de estado.
  \end{itemize}
\end{frame}

\begin{frame}{Modelado Matemático}
\begin{itemize}
  \item En el mundo idealizado de un sistema aislado, la medición exacta de las variables de estado determina de forma única el estado del sistema.
  \item Es posible que los parámetros que dan una descripción conveniente del sistema no sean directamente medibles. El modelo matemático describe entonces las relaciones entre las variables de estado.
  \item Utilizando estas relaciones, el estado del sistema a menudo puede determinarse a partir de mediciones factibles.
\end{itemize}
\end{frame}

\begin{frame}{Ejemplo: Sistema "pelota en un círculo"}
\begin{itemize}
  \item Ejemplo: Sistema simple de un "pelota en un círculo".
  \item El estado del sistema se describe por las coordenadas del pelota (x, y).
  \item Estas coordenadas están sujetas a la restricción $x^2 + y^2 = r^2$.
  \item Si la pelota se acerca al eje $x$, las criaturas unidimensionales que viven en el eje $x$ pueden observar su sombra.
  \item La sombra determina la coordenada $x$ de la pelota, y usando la ecuación ($x^2 + y^2 = r^2$) se puede obtener la coordenada $y$: $x=\pm\sqrt{r^2-y^2}$.
  \item Sin embargo, la signatura de la coordenada $y$ no se puede determinar con la información disponible.
  \end{itemize}
\end{frame}

\begin{frame}{Ejemplo: Sistema "pelota en un círculo"}
\begin{itemize}
  \item Si el eje $x$ está iluminada con luz roja desde arriba y luz azul desde abajo, entonces las criaturas unidimensionales pueden determinar la signatura de la coordenada $y$.
  \item Con esta información adicional, la ubicación de la pelota se puede determinar completamente.
  \item El conjunto de todos los pares ordenados de números reales que satisfacen la restricción $x^2 + y^2 = r^2$ es el espacio de estado del sistema.
  \item En este caso, el espacio de estado es un círculo de radio r centrado en (0, 0).
\end{itemize}
\end{frame}

\begin{frame}{Ejemplo para discutir}
Supongamos que en el ejemplo anterior la pelota está atada a (0, 0) por una cuerda de longitud r.
\begin{itemize}
  
  \item ¿Qué relaciones satisfacen las variables de estado (x, y)? 
  \item ¿Hay una medición que los seres lineales puedan hacer para determinar la ubicación de la pelota? 
  \item ¿Cuál es el espacio de estado para este sistema?
  \end{itemize}
\end{frame}

\begin{frame}{Grados de libertad: Espacios vectoriales}
\begin{itemize}
  \item Los sistemas físicos se pueden modelar matemáticamente como conjuntos de ecuaciones y desigualdades.
  \item El estado de un sistema se puede describir mediante un conjunto de variables de estado.
  \begin{itemize}
  \item El conjunto de variables de estado puede ser finito, como un $R^n$, o infinito.
    \item Finitos grados de libertad o infinitos grados de libertad. 
    \item Recordar espacios de Hilbert.
  \end{itemize}
  
 \end{itemize}
\end{frame}

\begin{frame}{Espacios vectoriales}
Dados los vectores de $R^n$, $\mathbf{x}=(x_1,x_2, \dots , x_n)$ y el producto escalar $\langle \mathbf{x},\mathbf{y} \rangle=\sum_i x_iy_i$, verifique las siguientes propiedades:
\begin{itemize}
    \item $\langle \mathbf{x},\mathbf{y} \rangle=\langle \mathbf{y},\mathbf{x} \rangle$.
    \item $\langle a\mathbf{x},\mathbf{y} \rangle=a\langle \mathbf{x},\mathbf{y} \rangle$.
    \item $\langle \mathbf{x}+\mathbf{z},\mathbf{y} \rangle=\langle \mathbf{x},\mathbf{y} \rangle + \langle \mathbf{z},\mathbf{y} \rangle$.
  \end{itemize}
Se define la norma como de costumbre $||\mathbf{x}||=\sqrt{\langle \mathbf{x},\mathbf{x} \rangle}$. Verifique
\begin{itemize}
    \item $||a\mathbf{x}||=|a|~||\mathbf{x}||$.
    \item A partir de la siguiente función $F(t)=\langle \mathbf{x}+t\mathbf{y},\mathbf{x}+t\mathbf{y} \rangle$, que cumple la propiedad $F(t)\geq 0$, demuestre la desigualdad de Cauchy-Schwarz: $|\langle \mathbf{x},\mathbf{y}\rangle|\leq ||\mathbf{x}||~||\mathbf{y}||$.
  \end{itemize}
\end{frame}

\begin{frame}{Grados de libertad: ligaduras}
\begin{itemize}
	\item Espacio de estado
	\begin{itemize}
	\item El espacio de estado es el conjunto de todos los puntos que representan estados posibles del sistema.
    \item  Recordar el concepto de ligadura.
	\end{itemize}
  
  \item Las ecuaciones y desigualdades que describen el sistema se denominan modelo matemático del sistema.
  
\end{itemize}
\end{frame}



\begin{frame}{Grados de libertad y aproximación lineal}
\begin{itemize}
\item Linealidad
\begin{itemize}
	\item Los sistemas lineales tienen un modelo matemático que consiste en un conjunto de ecuaciones lineales.
  \item Los sistemas no lineales pueden aproximarse por sistemas lineales mediante el uso de la aproximación de Taylor.
  \end{itemize}
  
  \item Los sistemas físicos a menudo se dividen en variables de entrada y variables de salida.
  \item Las variables de entrada son las variables que se controlan, y las variables de salida son las variables que se miden.
  \item El modelo matemático del sistema se puede usar para determinar las variables de estado a partir de las variables de salida (problema inverso)
\end{itemize}
\end{frame}
\begin{frame}{Un toy model}
\begin{itemize}
    \item Determinación de la altura de una montaña, usando la distancia a la base y el ángulo subtendido hasta la cumbre desde un punto de observación $P$.
    \begin{itemize}
        \item ¿Cuáles son las variables de estado? ($d$,$h$,$\theta$,$l$)
        \item ¿Cuáles son las ecuaciones que las relacionan?
        \item ¿Cómo cuantificar el efecto de los errores de medición en las predicciones del modelo?
    \end{itemize}
\end{itemize}
\centering
\begin{tikzpicture}

\draw (0,0) -- (10,0);
\draw[green] (6,0) -- (8,4) -- (10,0);
\draw[blue,dashed] (0,0) -- (8,4);
\draw[<->,red,dashed] (0,-.2) -- (8,-.2);
\draw[<->,red,dashed] (8,0) -- (8,4);

\node[anchor=north] at (4,-.3) {$d$};
\node[anchor=north] at (8.5,2) {$h$?};
\node[anchor=east] at (1.1,0.22) {$\theta$};
\node[anchor=east] at (0,0.26) {$P$};

\end{tikzpicture}
\end{frame}
\begin{frame}{Un toy model}
\centering
\begin{tikzpicture}

\draw (0,0) -- (10,0);
\draw[green] (6,0) -- (8,4) -- (10,0);
\draw[blue,dashed] (0,0) -- (8,4);
\draw[blue,dashed] (3.5,0) -- (8,4);
\draw[<->,blue,dashed] (0,-.2) -- (3.5,-.2);
\draw[<->,red,dashed] (8,0) -- (8,4);

\node[anchor=north] at (1.75,-.3) {$d$};
\node[anchor=north] at (8.5,2) {$h$?};
\node[anchor=east] at (1.2,0.22) {$\theta_1$};
\node[anchor=east] at (0,0.26) {$P_1$};
\node[anchor=east] at (4.5,0.22) {$\theta_2$};
\node[anchor=east] at (3.5,0.26) {$P_2$};

\end{tikzpicture}
\end{frame}
\begin{frame}{Algunos problemas del modelo}
\begin{itemize}
    \item   La forma de la montaña puede no ser como se ve en la figura. 
    
    Es imposible tener una linea de visión con la cima de la montaña.
    \item  La curvatura de la Tierra no se tiene en cuenta en el modelo matemático.
    
    Esto puede causar errores si las distancias involucradas son grandes.
    \item  La geometría de las mediciones puede ser diferente de la geometría euclidiana utilizada en el modelo matemático. 
    
    Esto se debe a que la luz puede ser refractada por la atmósfera.

    \item Este tipo de problemas aparecen en la generación de imágenes médicas. 
    \end{itemize}
\end{frame}
\begin{frame}{Algunos problemas del modelo}
    
\begin{tikzpicture}

\draw (0,0) -- (10,0);
\draw[green,dashed] (10,0) arc(0:180:3);


\draw[blue,dashed] (0,0) -- (5.71,2.71);
\draw[blue,dashed] (0,0) arc(-90:-45:6.67);

\draw[<->,red,dashed] (4.78,0) -- (4.78,2.02);
\draw[<->,red,dashed] (5.71,0) -- (5.71,2.71);
\draw[<->,red,dashed] (7,0) -- (7,3);

\end{tikzpicture}

\end{frame}

\begin{frame}{Otro toy model}
Se desea determinar la profundidad de dos capas de líquido de índices de refracción $n_1$ y $n_2$. Se conocen los índices, la profundidad total $h$ y la distancia $l$.

\centering
\begin{tikzpicture}
\draw (5,4.5) -- (6.5,5.5);
\draw (4.5,3) -- (5,4.5);
\draw (2.75,0) -- (4.5,3);

\draw (0,4.5) -- (10,4.5);
\draw (0,3) -- (10,3);
\draw (0,0) -- (10,0);

\draw[dashed] (5,0) -- (5,4.5);
\draw[dashed] (4.5,0) -- (4.5,4.5);

\draw[<->,red,dashed] (2,3) -- (2,4.5);
\draw[<->,red,dashed] (2,0) -- (2,3);

\node[anchor=east] at (2,3.75) {$h_1$};
\node[anchor=east] at (2,1.75) {$h_2$};

\node[anchor=east] at (5.7,4) {$\theta_1$};
\node[anchor=east] at (4.5,1.75) {$\theta_2$};

\node[anchor=east] at (8,3.75) {$n_1$};
\node[anchor=east] at (8,1.75) {$n_2$};

\draw[<->,blue,dashed] (9,0) -- (9,4.5);
\node[anchor=west] at (9,2.5) {$h$};

\draw[<->,blue,dashed] (2.75,-.2) -- (5,-.2);
\node[anchor=west] at (3.75,-.5) {$l$};
\end{tikzpicture}

\end{frame}

\begin{frame}{Tarea 01}
Ejercicios 1.1.3, 1.1.7, 1.1.8, 1.1.9, 1.1.13 y 1.1.14
\end{frame}

\end{document}
