# Clase 12: Filtros 2
## Resolución
* La resolución es una distancia asociada a un filtro. 
* $\mathcal{R_A}$ es la resolución asociada al filtro $\mathcal{A}$
* Mayor resolución significa menor $\mathcal{R_A}$ y viceversa.
* Esta distancia se interpreta de distintas formas
    * El tamaño mínimo de una característica discernible en la salida.
    * La separación mínima entre dos puntos con características identificables.
    * La extensión que adquiere un objeto puntual en la imagen.

## FW $\kappa$ M
* Suponemos un filtro a partir de una función con un máximmo central, que decae en ambas direcciones.

* El parámetro $\kappa$ indica la altura de corte donde se considera termina la dispersión de un valor puntual.

* Si $\kappa=0.5$ es el caso FWHM.

## Ejemplo

* Función rectángulo:

$$k=(2d)^{-1}\chi_{\left[ -d,d \right]}.$$

* La resolución siempre es $2d$

$$\Delta_{\mathcal{A_d,\kappa}}= 2d.$$

* Función tienda:

$$t_d=\left\{\begin{array}{cc} 0 & |t|\ge d \\ d^{-2}(d+t) & -d<t<0 \\ d^{-2}(d-t) & 0 \le t < d\end{array} \right. .$$

* La resolución depende de $\kappa$

$$\Delta_{\mathcal{A_d,\kappa}}= 2(1-\kappa)d.$$

* Discutir.

## Otras definiciones

* Primer cero:
    * Si la función de promedio (spread function) tiene ceros, se puede usar la posición de los primeras raices más cercanas al máximo.
* Ancho equivalente
    * Si la función $k$ tiene su máximo en 0 y es integrable:

$$\Delta_{\mathcal{A_d,ew}}=\frac{\hat k(0)}{k(0)}.$$

* Recordar que $\hat k(0)$ es la integral de $k$ en todo el espacio.


## Filtros periodicos

* Un filtro $L-$periódico convierte funciones $L-$periódicas en funciones $L-$periódicas.

* No lo dijimos antes, pero dado un filtro lineal invariante de traslaciones $\mathcal{A}$ se define su respuesta singular (impulse response):

$$k(t)=\mathcal{A}(\delta(t)).$$

* $\mathcal{A}x$ es la convolución con $k$. 

* En el caso de un filtro $L-$periódico corresponde definir el impulso peródico (tren de deltas).

$$\delta_L(t)=\sum_{n=-\infty}^{\infty}\delta(t-nL).$$

## Función de transferencia periódica

* La respuesta singular será:

$$k(t)=\mathcal{A}(\delta_L(t)).$$

$$\mathcal{A}f(t) = \int_{0}^{L}k(t-s)f(s).$$


* La función de transferencia periódica es la serie de Fourier de $k$.

* La representación de Fourier del filtro $L-$periódico

$$\mathcal{A}x(t) = \frac{1}{L}\sum_{n=-\infty}^{\infty}\hat x(n)\hat k(n)e^{\frac{2\pi int}{L}}.$$


## Aplicaciones de filtros al procesamiento de imágenes

* Veremos algunos ejemplos de filtros con diferentes funciones:
    * Identificar propiedades de una imagen
    * Corregir distorsiones
    * Resaltar características
* Este filtrado ocurre luego de hacer las medidas.
* Es postprocesamiento.
* Hay procesos instrumentales de medida que también se tratan como filtros. 

## Transformaciones de coordenadas

* El instrumento parametriza la información 2-dimensional de la imagen en coordenadas $(x_1,x_2)$ (\textit{plano de medida}).

* La medida es un conjunto de una (escala de gris) o varias (imagen a color) funciones escalares $f(x_1,x_2)$.

* La imagen en el \textit{plano de imagen} es una función de las coordenadas $(y_1,y_2)$.

* Si evaluamos $f$ directamente en las coordendadas de la imagen $(y_1,y_2)$ obtenemos una imagen distorsionada.

* La transformación de coordenadas define un filtro

$$\Phi: (y_1,y_2) \longrightarrow (x_1,x_2) = (g(y_1,y_2),h(y_1,y_2)).$$


$$\mathcal{A}_{\Phi}f(y_1,y_2) = f\circ \Phi(y_1,y_2) = f(g(y_1,y_2),h(y_1,y_2)).$$


## Reducción de ruido

* El ruido uniforme es una variación uniforme de la función y de promedio nulo.
* Se puede corregir mediante un promedio.
* Se usa una función no negativa de área total 1

$$\int_{-\infty}^{\infty}\varphi(t)dt=1.$$

* La función $\varphi$ suele ser una campana de cierto ancho. En 2D se escoge de forma isotrópica.


## Pérdida de contraste en filtro de ruido

* Este filtro $\mathcal{A}_{\varphi}$ es naturalmente un filtro pasa bajo.
    * Limpia el ruido.
    * Baja el contraste y la resolución.

* Discutir.

* Si el ruido de la imagen no está distribuido de forma uniforme a lo largo de la misma, se puede usar un parámetro $|\mu|\le 1$  que ajuste la importancia del promedio:

$$\mathcal{A}_{\varphi,\mu}f = \mu\mathcal{A}_{\varphi}f + (1-\mu)f$$


## Aumento de contraste

* En algunas circunstancias se puede aumentar el contraste de una imagen.
* Filtro pasa altas.
* En la representación de frecuencia:

$$\mathcal{A}_{\varphi}f = \mathcal{F}^{-1}\left[\hat\varphi\hat f\right].$$

* La función de transferencia $\varphi$ debe resaltar las altas frecuencias

$$\hat\varphi(0)=0.$$

$${\lim \atop {|\xi|\rightarrow \infty}} \hat\varphi(\xi) =1.$$

## Detección de bordes

* Hay varias estrategias para la detección de bordes
    * Los bordes son cambios abruptos.
    * Buscamos grandes valores del gradiente de la función de la imagen. Mayores a un cierto umbral $t_{borde}$
    * Idea

$$\mathcal{A}_1f=\chi_{\left[t_{borde},\infty\right)}(|\nabla f|)$$

## Detección de bordes 2

* El muestreo del gradiente detectar falsos bordes si la imagen es muy rugosa.
* Se hace un promedio.

$$\mathcal{A}_2f=\chi_{\left[t_{borde},\infty\right)}(|\nabla (\varphi \star f)|)$$

* El muestreo también puede no ver los bordes si la textura es muy parecida a ambos lados.
* Se considera un promedio sobre la magnitud del gradiente o sobre la magnitud del gradiente de la función promediada.

$$\mathcal{A}_3f=\chi_{\left[t_{borde},\infty\right)}\left(\frac{|\nabla f|}{\psi\star|\nabla f)|}\right)$$

## Ejercicios

* Ejercicio 9.4.4


