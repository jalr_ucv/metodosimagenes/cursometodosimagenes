\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}
\usepackage{pgfplots}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 10: Sampling y +}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{12 de octubre de 2023} % Date, can be changed to a custom date

\begin{document}

\frame{\titlepage}

\begin{frame}{Introducción}
    \begin{itemize}
        \item La imagen es una función.
        \begin{itemize}
            \item Toy model: función en $\mathbb{R}$ (un continuo).
            \item Imagen en $\mathbb{R}$, $\mathbb{C}$ o $\mathbb{R}^n$.
        \end{itemize}
        \item Sólo conoceremos el valor de la función en un conjunto finito de puntos. Esto es una muestra (sample).
        \item Los valores del par $(x, f(x))$ están redondeados a un conjunto de valores digitales posibles predefinidos. Esto es un proceso de cuantización.
        \item Relacionaremos transformada y serie de Fourier.
        \begin{itemize}
            \item Transformada es ok en la parte abstracta.
            \item La serie aparece al restringir a un intervalo finito.
            \item Serie y transformada deben aproximarse a sumatorias finitas sobre conjuntos de puntos.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Muestreo (sampling)}
    \begin{itemize}
        \item Las medidas son la evaluación de una función en un punto o en un conjunto de puntos.
        \item Un conjunto de puntos $\lbrace x_i \rbrace$ de un intervalo $(a, b)$ es discreto \textit{si no se acumula}.
        \begin{itemize}
            \item Ninguna secuencia de puntos $x_j$ converge a un punto en el intervalo.
        \end{itemize}
        \item Dada una función $f$ en el intervalo $(a, b)$, un conjunto de puntos $\lbrace x_i \rbrace$ se denominan puntos de muestreo y los valores $f(x_i)$ se denominan muestra de $f$.
        \item Es común que los puntos de muestreo estén equidistantes.
        \begin{itemize}
            \item $l = x_{i+j} - x_i$ es el espacio de muestreo.
            \item $l^{-1}$ es la frecuencia de muestreo.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Corte de alta frecuencia}
    \begin{itemize}
        \item Una medida realista de $f$ implica una convolución
        $$f\star\varphi.$$
        \item $\varphi$ es una función continua que contiene la información del aparato de medida.
        \begin{itemize}
            \item Recordar el promedio móvil.
        \end{itemize}
        \item $\varphi$ es una función continua, por lo tanto $\hat\varphi$ tiende a cero a altas frecuencias.
        \item Se entiende que $f\star\varphi$ no conserva una componente de alta frecuencia.
    \end{itemize}
\end{frame}

\begin{frame}{Límite de frecuencia y ancho de frecuencia}
    \begin{itemize}
        \item Sea $f$ una función cuya transformada $\hat f$ tiene soporte compacto.
        \begin{itemize}
            \item $f$ es una función de frecuencia limitada. Si $\hat f \neq 0$ en $\left[-L,L \right]$, tiene límite de frecuencia $L$.
            \item $f$ es una función de ancho de frecuencia limitada. Si $\hat f \neq 0$ en un intervalo de ancho $W$, $f$ tiene ancho de frecuencia $W$.
        \end{itemize}
        \item $$f(x)=\frac{1}{2\pi}\int_{-L}^{L}\hat f(\xi)e^{ix\xi}d\xi.$$
    \end{itemize}
\end{frame}

\begin{frame}{Teorema de Nyquist}
    \begin{itemize}
        \item Sea $f$ de cuadrado integrable y con límite de frecuencia $L$, entonces $f$ puede ser determinada con un muestreo de espacio $\frac{\pi}{L}$.
        \item Por ejemplo, $$f(\frac{n\pi}{L}),~~n\in \mathbb{Z}.$$
        \item El ancho de la frecuencia determina el espacio de muestreo. Comentar.
    \end{itemize}
\end{frame}

\begin{frame}{Interpolación de Shannon-Whittaker}
    \begin{itemize}
        \item Si $f$ decae en infinito suficientemente rápido para que,
        $$\sum_{n=-\infty}^{\infty}\left|f(\frac{n\pi}{L})\right|<\infty$$
        \item Se puede obtener la siguiente expresión de interpolación
        $$f(x)=\sum_{n=-\infty}^{\infty}f(\frac{n\pi}{L})\mathrm{sinc}(Lx-n\pi).$$
        \item Es exacta para una función con ancho de frecuencia $L$, pero requiere una suma infinita.
        \item En la práctica se usa un número finito de términos, pero la convergencia no es buena (necesitamos muchos términos).
        \item El desempeño de la interpolación se mejora con un sobremuestreo.
    \end{itemize}
\end{frame}

\begin{frame}{Fórmula de suma de Poisson}
    \begin{itemize}
        \item $f$ es una función continua que decae rápidamente en infinito. Supongamos que es absolutamente integrable.
        \item Definimos una función periódica en el intervalo $\left[0,1\right]$:
        $$f_p(x)=\sum_{n=-\infty}^{\infty}f(x+n).$$
        \item $f_p(x)$ es absolutamente integrable en $\left[0,1\right]$.
        \item Si se cumple $\sum_{n=-\infty}^{\infty}|\hat f (2\pi n)| < \infty$, entonces
        $$\sum_{n=-\infty}^{\infty}f(x+n)=\sum_{n=-\infty}^{\infty}\hat f (2\pi n)e^{2\pi i nx}.$$
    \end{itemize}
\end{frame}

\begin{frame}{Comentarios suma de Poisson}
    \begin{itemize}
        \item Generalización a un intervalo $\left[-L,L\right]$,
        $$\sum_{n=-\infty}^{\infty}f(x+2nL)=\frac{1}{2L}\sum_{n=-\infty}^{\infty}\hat f (\frac{\pi n}{L})e^{\frac{\pi i nx}{L}}.$$
        \item Función de soporte acotado en $\left[-L,L\right]$ se reproduce a partir de una muestra discreta de la transformada de Fourier.
        $$f(x)=\frac{1}{2L}\sum_{n=-\infty}^{\infty}\hat f (\frac{\pi n}{L})e^{\frac{\pi i nx}{L}}.$$
        \item Una función de soporte acotado en $\left[-L,L\right]$ impone un mínimo espaciamiento en frecuencia $\sim \pi/L$.
    \end{itemize}
\end{frame}

\begin{frame}{Fórmula de suma dual de Poisson}
    \begin{itemize}
        \item Se puede desarrollar la expresión para la transformada de Fourier
        $$\sum_{n=-\infty}^{\infty}\hat f(\xi+2nL)=\frac{\pi}{L}\sum_{n=-\infty}^{\infty} f (\frac{\pi n}{L})e^{-\frac{\pi i n\xi}{L}}.$$
    \end{itemize}
\end{frame}

\begin{frame}{Solapamiento (Aliasing) I}
    \begin{itemize}
        \item Efecto del muestreo. Tomemos la interpolación de Shannon-Whittaker para una función arbitraria
        $$F_L(x)=\sum_{n=-\infty}^{\infty}f(\frac{n\pi}{L})\mathrm{sinc}(Lx-n\pi).$$
        \item Ahora $F_L$ es una versión de ancho de frecuencia $L$ de la función $f$. La transformada de Fourier:
        $$\hat F_L(\xi)=\sum_{n=-\infty}^{\infty}\hat f(\xi + 2nL)\chi_{\left[-L,L\right]}(\xi).$$
    \end{itemize}
\end{frame}

\begin{frame}{Solapamiento (Aliasing) II}
    \begin{itemize}
        \item Error de reconstrucción para $\xi\notin \left[-L,L\right]:$
        $$\hat f(\xi)-\hat F_L(\xi)=\hat f(\xi)$$
        \item Desaparecen las frecuencias altas.
        \item Error de reconstrucción para $\xi\in \left[-L,L\right]:$
        $$\hat f(\xi)-\hat F_L(\xi)=-\sum_{n\neq0}\hat f(\xi + 2nL)$$
        \item Las frecuencias altas se disfrazan de frecuencias bajas (aliasing).
    \end{itemize}
\end{frame}

\begin{frame}{Ejercicios}
    \begin{itemize}
        \item Ejercicio 8.1.6
        \item Leer la sección 8.2.3 y obtener la ecuación 8.18
    \end{itemize}
\end{frame}

\end{document}