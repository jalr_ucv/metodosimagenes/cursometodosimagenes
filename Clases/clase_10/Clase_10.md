# Clase 10: Sampling y +
## Introducción

* La imagen es una función.
    * Toy model: función en R (un continuo)
    * Imagen en R, C o Rn
* Sólo conoceremos el valor de la función en un conjunto finito de puntos. Esto es una muestra (sample)
* Los valores del par x,f(x) están redondeados a un conjunto de valores digitales posibles predefinidos. Esto es un proceso de cuantización.
* Relacionaremos transformada y serie de Fourier.
    * Transformada es ok en la parte abstracta.
    * La serie aparce al restringir a un intervalo finito.
    * Serie y transformada deben aproximarse a sumatorias finitas sobre conjuntos de puntos.

## Muestreo (sampling)
* Las medidas son la evaluación de una función en un punto o en un conjunto de puntos.
* Un conjunto de puntos $\lbrace x_i\rbrace$ de un intervalo $(a,b)$ es discreto \textit{si no se acumula}.
    * Ninguna secuencia de puntos $x_j$ converge a un punto en el intervalo.
* Dada una función $f$ en el intervalo $(a,b)$. Un conjunto de puntos $\lbrace x_i\rbrace$ se denominan puntos de muestreo y los valores $f(x_i)$ se denominan muestra de $f$.
* Es común que los puntos de muestreo estén equidistantes.
    * $l=x_{i+j}-x_i$ es el espacio de muestreo.
    * $l^{-1}$ es la frecuencia de muestreo.

## Corte de alta frecuencia

* Una medida realista de $f$ implica una convolución

$$f\star\varphi.$$

* $\varphi$ es una función continua que contiene la información del aparato de medida. Recordar el promedio móvil.

* $\varphi$ es una función contínua, por lo tanto $\hat\varphi$ tiende a cero a altas frecuencias.

* Se entiende que $f\star\varphi$ no conserva un componente de alta frecuencia.

## Límite de frecuencia y ancho de frecuencia

* Sea $f$ una función cuya transformada $\hat f$ tiene soporte compacto.
    * $f$ es una función de frecuencia limitada. Si $\hat f \neq 0$ en $\left[-L,L \right]$, tiene límite de frecuencia $L$.
    * $f$ es una función de ancho de frecuencia limitada. Si $\hat f \neq 0$ en un intervalo de ancho $W$, $f$ tiene ancho de frecuencia $W$.

$$f(x)=\frac{1}{2\pi}\int_{-L}^{L}\hat f(\xi)e^{ix\xi}d\xi.$$


## Teorema de Nyquist

* Sea $f$ de cuadrado integrable y con límite de frecuencia $L$, entonces $f$ puede ser determinada con un muestreo de espacio $\frac{\pi}{L}$.

* Por ejemplo

$$f(\frac{n\pi}{L}),~~n\in Z.$$

* El ancho de la frecuencia determina el espacio de muestreo. Comentar.

## Interpolación de Shannon-Whittaker

* Sí $f$ decae en infinito suficientemente rápido para que,

$$\sum_{n=-\infty}^{\infty}\left|f(\frac{n\pi}{L})\right|<\infty$$

* Se puede obtener la siguiente expresión de interpolación

$$f(x)=\sum_{n=-\infty}^{\infty}f(\frac{n\pi}{L})\mathrm{sinc}(Lx-n\pi).$$

* Es exacta para una función con ancho de frecuencia $L$, pero requiere una suma infinita.
* En la práctica se usa un número finito de términos, pero la convergancia no es buena (necesitamos muchos términos)
* El desempeño de la interpolación se mejora con un sobremuestreo.

## Fórmula de suma de Poisson

* $f$ es una función continua que decae rápidamente en infinito. Supongamos que es absolutamente integrable.

* Definimos una función periodica en el intervalo $\left[0,1\right]$:

$$f_p(x)=\sum_{n=-\infty}^{\infty}f(x+n).$$

* $f_p(x)$ es absolutamente integrable en $\left[0,1\right]$.

* Si se cumple $\sum_{n=-\infty}^{\infty}|\hat f (2\pi n)| < \infty$, entonces

$$\sum_{n=-\infty}^{\infty}f(x+n)=\sum_{n=-\infty}^{\infty}\hat f (2\pi n)e^{2\pi i nx}$$

## Comentarios suma de Poisson

* Generalización a un intervalo $\left[-L,L\right]$,

$$\sum_{n=-\infty}^{\infty}f(x+2nL)=\frac{1}{2L}\sum_{n=-\infty}^{\infty}\hat f (\frac{\pi n}{L})e^{\frac{\pi i nx}{L}}.$$

* Función de soporte acotado en $\left[-L,L\right]$ se reproduce a partir de una muestra discreta de la transformada de Fourier.

$$f(x)=\frac{1}{2L}\sum_{n=-\infty}^{\infty}\hat f (\frac{\pi n}{L})e^{\frac{\pi i nx}{L}}.$$

* Una función de soporte acotado en $\left[-L,L\right]$ impone un mínimo espaciamiento en fracuencia $\sim \pi/L$.

## Fórmula de suma dual de Poisson

* Se puede desarrollar la expresión para la transformada de Fourier

$$\sum_{n=-\infty}^{\infty}\hat f(\xi+2nL)=\frac{\pi}{L}\sum_{n=-\infty}^{\infty} f (\frac{\pi n}{L})e^{-\frac{\pi i n\xi}{L}}.$$

## Solapamiento (Aliasing) I

* Efecto del muestreo. Tomemos la interpolación de Shannon-Whittaker para una función arbitraria

$$F_L(x)=\sum_{n=-\infty}^{\infty}f(\frac{n\pi}{L})\mathrm{sinc}(Lx-n\pi).$$

* Ahora $F_L$ es una versión de ancho de frecuencia $L$ de la función $f$. La transformada de Fourier:

$$\hat F_L(\xi)=\sum_{n=-\infty}^{\infty}\hat f(\xi + 2nL)\chi_{\left[-L,L\right]}(\xi).$$

## Solapamiento (Aliasing) II

* Error de reconstrucción para $\xi\notin \left[-L,L\right]:$

$$\hat f(\xi)-\hat F_L(\xi)=\hat f(\xi)$$

* Desaparecen las frecuencias altas.

* Error de reconstrucción para $\xi\in \left[-L,L\right]:$

$$\hat f(\xi)-\hat F_L(\xi)=-\sum_{n\neq0}\hat f(\xi + 2nL)$$

* Las frecuencias altas se disfrazan de frecuencias bajas (aliasing).

* Discutir.

## Ejercicios

* Ejercicio 8.1.6
* Leer la sección 8.2.3 y obtener la ecuación 8.18