\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}
\usepackage{pgfplots}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 4: Un modelo de tomografía básico}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{17 de agosto de 2023} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Introducción}
\begin{itemize}
  \item Introducción al modelo matemático para hacer tomografías
  \item Ley de Beer: interacción de fotones con el tejido
  \item Función densidad
  \item Definición de la transformada de Radón
  \item Inversión directa de la transformada de Radón
\end{itemize}
\end{frame}

\begin{frame}{Tomografía}
\begin{itemize}
  \item Tomografía es imagen en láminas
  \item Objetivo: reconstruir una estructura interna a partir de medidas externas.
    \begin{itemize}
      \item Esa estructura interna está ligada a la física que se usa para medirla.
      \item Es una forma de densidad. Puede estar relacionada con la presencia de materia, pero puede tener otras propiedades asociadas.
      \item El objeto de interés al usar rayos X es el coeficiente de atenuación. Está ligado a la presencia de materia, pero depende de la sección eficaz de interacción de los fotones con los diferentes elementos presentes, la energía, etc.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Coeficiente de atenuación $\mu(\mathbf{x})$}
\begin{itemize}
  \item Es un número no negativo.
  \item Es nulo en el vacío.
  \item Es finito (positivo) el tejido
  \item Es suficientemente bajo en el aire / tejido. Se considera nulo en el aire.
  \item Al hacer tomografía con rayos X esperamos formar una imagen de ``coeficientes de atenuación''.
\end{itemize}
\end{frame}

\begin{frame}{Unidades Hounsfield}
\begin{itemize}
  \item El coeficiente de atenuación no es la unidad utilizada.
  \item Se usa la Unidad Hounsfield
  \item $$H_x = \frac{\mu_x-\mu_{agua}}{\mu_{agua}}\times 1000.$$
  \item Algunas unidades
\end{itemize}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Material & Coeficiente de atenuación en unidades Hounsfield \\
\hline
water & 0 \\
air & -1000 \\
bone & 1086 \\
blood & 53 \\
fat & -61 \\
brain white/gray & -4 \\
breast tissue & 9 \\
muscle & 41 \\
soft tissue & 51 \\
\hline
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Resolución}
\begin{itemize}
  \item Notar
    \begin{itemize}
      \item $H_{hueso} \sim 1100$
      \item $H_{aire} = -1000$
      \item $H_{tejidos} \sim (0,60)$
      \item $H_{tejidos + grasa} \sim (-60,60)$
    \end{itemize}
  \item Las diferencias entre las densidades de los tejidos blandos y sangre  son del orden del 3\% del rango total. Si se incluye la grasa es del orden del 6\%. 
  \item Eso quiere decir que la técnica para reconstruir la imagen de densidades debería acercarse a ese poder de resolución. Discutir.
\end{itemize}
\end{frame}

\begin{frame}{Definiciones}
\begin{itemize}
  \item Definimos la imagen de la \textit{slice} $c$
  \item $$f_c(x_1,x_2)=\mu(x_1,x_2,x_3=c)$$
  \item El sistema de referencia está determinado por la máquina.
  \begin{itemize}
    \item $x_3$ es el eje.
    \item $x_1$ puede estar en dirección horizontal
    \item $x_2$ puede estar en dirección vertical
  \end{itemize}
  \item Si reconstruimos $f_c(x_1,x_2)$ para todos los valores de $c$, reconstruimos la imagen.
\end{itemize}
\end{frame}

\begin{frame}{John}
\begin{figure}
\includegraphics[scale=.4]{imagenes/john_ICRP.png}
\end{figure}
\end{frame}

\begin{frame}{Función característica}
\begin{itemize}
  \item Dado un conjunto del plano $D$ se define su función característica $\chi_D$
  \item $$
    \chi_D=\left\{ \begin{array}{cc} 1 & x\in D \\ 0 & x\notin D \end{array} \right.
    $$
  \item $\chi_D$ es el caso de una densidad constante dentro del material
\end{itemize}
\end{frame}

\begin{frame}{Otro modelo de juguete de densidad}
\begin{itemize}
  \item Otro modelo de densidad
  \item $$
    \mu(\mathbf{x})=\left\{ \begin{array}{cc} 1-||\mathbf{x}|| & \mathbf{x}\leq 1 \\ 0 & \mathbf{x}> 1 \end{array} \right.
    $$
  \item Las \textit{slices} quedan así
  \item $$
    f_c(x_1,x_2)=\left\{ \begin{array}{cc} 1-\sqrt{x_1^2+x_2^2+c^2} &  si ~\sqrt{x_1^2+x_2^2+c^2}\leq 1 \\ 0 & si ~\sqrt{x_1^2+x_2^2+c^2}> 1 \end{array} \right.
    $$
\end{itemize}
\end{frame}

\begin{frame}{Ley de Beer}
\begin{itemize}
  \item Determina la disminución de la intensidad de un haz de rayos X al atravesar la materia
  \item $$\frac{dI}{ds}=-\mu(\mathbf{x}(s))I.$$
  \item $s$ va en la dirección del haz.
  \item Un modelo sencillo de interacción de los rayos X en la materia usa la Ley de Beer y dos suposiciones más
  \begin{itemize}
    \item No hay refracción o difracción
    \item Los rayos X son monocromáticos
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Atenuación en un rayo (Problema directo)}
\begin{itemize}
  \item Definimos un rayo 
  \item $$\mathbf{r}(s)=\mathbf{r}_0+\mathbf{v}s.$$
  \item Queremos escribir la intensidad en función de $s$ sobre el rayo.
  \item Hay dos puntos de referencia inicial y final $s=a$, $s=b$.
  \item Conocemos el coeficiente de atenuación $\mu(\mathbf{x})$.
  \item Discutir.
\end{itemize}
\end{frame}

\begin{frame}{Atenuación de una fuente puntual (planilandia)}
\begin{itemize}
  \item Consideremos una fuente puntual en el plano
  \item Los haces se separan en forma de abanico.
  \item La intensidad decae como $1/r$. Recordar ley de Gauss.
  \item Se puede asignar un $\mu$ a este efecto.
  \begin{itemize}
    \item ¿Cómo? ¿Cuánto vale $\mu$?
  \end{itemize}
  \item Cuando el efecto de dispersión del haz no es relevante en la aplicación, tenemos un haz \textbf{no divergente}.
  \begin{itemize}
    \item ¿Cuándo ocurre esto?
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Ley de Beer y sombras de objetos}
\begin{itemize}
  \item Usando la Ley de Beer se puede calcular la función de sombra de objetos.
  \item Es un problema directo completo.
  \item Discutir.
  \item ¿Conocen modelos matemáticos para encontrar la función de sombra?
\end{itemize}
\end{frame}

\begin{frame}{Transformación de una imagen bidimensional}
\begin{itemize}
  \item Phantom matemático: Shepp-Logan
  \begin{itemize}
    \item 10 elipses en un cuadrado (-1,-1) - (1,1)
  \end{itemize}
\end{itemize}
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Ellipse & Center & Major Axis & Minor Axis & Theta & Gray Level \\
\hline
a & (0,0) & 0.69 & 0.92 & 0 & 2 \\
b & (0,-0.0184) & 0.6624 & 0.874 & 0 & -0.98 \\
c & (0.22,0) & 0.11 & 0.31 & -18$^\circ$ & -0.02 \\
d & (-0.22,0) & 0.16 & 0.41 & 18$^\circ$ & -0.02 \\
e & (0,0.35) & 0.21 & 0.25 & 0 & 0.01 \\
f & (0,0.1) & 0.046 & 0.046 & 0 & 0.01 \\
g & (0,-0.1) & 0.046 & 0.046 & 0 & 0.01 \\
h & (-0.08,-0.605) & 0.046 & 0.023 & 0 & 0.01 \\
i & (0,-0.605) & 0.023 & 0.023 & 0 & 0.01 \\
j & (0.06,-0.605) & 0.023 & 0.046 & 0 & 0.01 \\
\hline
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Shepp-Logan}
\begin{figure}
\input{imagenes/shepp-logan.tex}
\end{figure}
\end{frame}

\begin{frame}{Tomo}
\begin{itemize}
\item Podemos considerar la operación de recorrer el phantom y tomar las sombras en cada dirección.
\item ¿Cómo se vería la imagen?
\end{itemize}
\begin{figure}
\input{imagenes/haz_abanico.tex}
\end{figure}
\end{frame}

\begin{frame}{Transformada de Radon}
\begin{columns}[t] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\column{.5\textwidth} % Left column and width

\begin{itemize}
  \item Recordemos el espacio de los rayos
  \item $\langle \mathbf{r},\vec{\mathbf{\omega}}\rangle = t$
  \item Son los puntos $\mathbf{r}$ que proyectan un $t$ constante sobre el vector $\vec{\mathbf{\omega}}$.
  \item Los rayos son perpendiculares al vector $\vec{\mathbf{\omega}}$.
  \item $t$ es la distancia del rayo al origen. 
\end{itemize}

\column{.5\textwidth} % Right column and width

\center
\begin{tikzpicture}[use Hobby shortcut]
  \path
  (0.5,0.5) coordinate (z0)
  (1.7,1.3) coordinate (z1)
  (1.3,2.3) coordinate (z2)
  (.6,1.9) coordinate (z3)
  (.4,1.5) coordinate (z4);
  \draw[closed] (z0) .. (z1) .. (z2) .. (z3) .. (z4);
  \draw[->] (-1,0) -- (2, 0);
  \draw[->] (0,-1) -- (0, 3);
  
  \draw[-] (-1,-.5) -- (2.4, 1.2);
  
  \draw[->]  (-1.2, 2.4) -- (-.25,.5);
  \draw[->]  (-0.8, 2.6) -- (.15,.7);
  \draw[->]  (-0.4, 2.8) -- (.55,.9);
  \draw[->]  (0.0, 3.0) -- (0.95,1.1);
  \draw[->]  (0.4, 3.2) -- (1.35,1.3);
  \draw[->]  (0.8, 3.4) -- (1.75,1.5);
  \draw[->]  (1.2, 3.6) -- (2.15,1.7);
\end{tikzpicture}

\end{columns}

\end{frame}





\begin{frame}{Transformada de Radon}
\begin{itemize}
\item La transformada de Radon es

\begin{eqnarray}
\mathcal{R}f(t,\vec{\omega}) &=& \int_{l_{(t,\vec{\omega})}}f ds \nonumber \\
&=& \int_{-\infty}^{\infty}f(s\hat{\omega}+t\vec{\omega})ds\\
&=& \int_{-\infty}^{\infty}f(t\omega_1-s\omega_2,t\omega_2+s\omega_1)ds.\nonumber
\end{eqnarray}

\item ¿Es posible imaginar la transformada de Radon como un sistema lineal y plantear su inversa?
	
\end{itemize}
\end{frame}

\begin{frame}{Tarea}
\begin{itemize}
\item Ejercicio 3.4.2
\item $B_r(a)$ es una bola de radio $r$, centrada en el punto $a$. Ver definición 3.4.3.
\item Se define para cualquier dimensión. En este caso particular es $\mathcal{R}^2$.
\end{itemize}
\end{frame}

\end{document}


\begin{frame}{Tarea}
\begin{figure}
\input{imagenes/shepp-logan.tex}
\end{figure}

\begin{equation}
\chi_D=\left\{ \begin{array}{cc} 1 & x\in D \\ 0 & x\notin D \end{array} \right.
\end{equation}


\end{frame}