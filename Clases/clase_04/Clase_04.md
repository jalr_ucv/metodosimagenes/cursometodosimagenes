# Clase 4: Un modelo de tomografía básico
## Introducción

* Introducción al modelo matemático para hacer tomografías
* Ley de Beer: interacción de fotones con el tejido
* Función densidad
* Definición de la transformada de Radón
* Inversión directa de la transformada de Radón

## Tomografía
* Tomografía es imagen en láminas
* Objetivo: reconstruir una estructura interna a partir de medidas externas.
    * Esa estructura interna está ligada a la física que se usa para medirla. 
    * Es una forma de densidad. Puede estar relacionada con la presencia de materia, pero puede tener otras propiedades asociadas.
    * El objeto de interés al usar rayos X es el coeficiente de atenuación. Está ligado a la presencia de materia, pero depende de la sección eficaz de interacción de los fotones con los diferentes elementos presentes, la energía, etc.

## Coeficiente de atenuación
* Es un número no negativo.
* Es nulo en el vacío.
* Es finito (positivo) el tejido
* Es suficentemente bajo en el aire / tejido. Se considera nulo en el aire.
* Al hacer tomografia con rayos X esperamos formar una imagen de ``coeficientes de atenuación''.

## Unidades Hounsfield
* El coeficiente de atenuación no es la unidad utilizada.
* Se usa la Unidad Hounsfield

$$H_x = \frac{\mu_x-\mu_{agua}}{\mu_{agua}}\times 1000.$$

Algunas unidades

| Material | Coeficiente de atenuación en unidades Hounsfield |
| :-- | :-- |
| water | 0 |
| air | \-1000 |
| bone | 1086 |
| blood | 53 |
| fat | \-61 |
| brain white/gray | \-4 |
| breast tissue | 9 |
| muscle | 41 |
| soft tissue | 51 |

## Resolución

* Notar
    * $H_{hueso} \sim -1100$
    * $H_{aire} = 1000$
    * $H_{tejidos} \sim (0,60)$
    * $H_{tejidos + grasa} \sim (-60,60)$

* Las diferencias entre las densidades de los tejidos blandos y sangre  son del orden del 3\% del rango total. Si se incluye la grasa es del orden del 6 \%. 
* Eso quiere decir que la técnica para reconstruir la imagen de densidades debería acercarse a ese poder de resolución. Discutir.

## Definiciones
* Definimos la imagen de la \textit{slice} $c$

$$f_c(x_1,x_2)=\mu(x_1,x_2,x_3=c)$$

* El sistema de referencia está determinado por la máquina.
    * $x_3$ es el eje.
    * $x_1$ puede estar en dirección horizontal
    * $x_2$ puede estar en dirección vertical

* Si reconstruimos $f_c(x_1,x_2)$ para todos los valores de $c$, reconstruimos la imagen.

## Función característica

* Dado un conjunto del plano $D$ se define su función característica $\chi_D$

$$
\chi_D=\left\{ \begin{array}{cc} 1 & x\in D \\ 0 & x\notin D \end{array} \right.
$$

* $\chi_D$ es el caso de una densidad constante dentro del material

## Otro modelo de juguete de densidad
* Otro modelo de densidad

$$
\mu(\mathbf{x})=\left\{ \begin{array}{cc} 1-||\mathbf{x}|| & \mathbf{x}\leq 1 \\ 0 & \mathbf{x}> 1 \end{array} \right.
$$

* Las \textit{slices} quedan así

$$
f_c(x_1,x_2)=\left\{ \begin{array}{cc} 1-\sqrt{x_1^2+x_2^2+c^2} &  si ~\sqrt{x_1^2+x_2^2+c^2}\leq 1 \\ 0 & si ~\sqrt{x_1^2+x_2^2+c^2}> 1 \end{array} \right.
$$

## Ley de Beer
* Determina la disminución de la intensidad de un haz de rayos X al atravesar la materia

$$\frac{dI}{ds}=-\mu(\mathbf{x})I.$$

* Un modelo sencillo de interacción de los rayos X en la materia usa la Ley de Beer y dos suposiciones más
    * No hay refracción o difracción
    * Los rayos X son monocromáticos

## Atenuación en un rayo (Problema directo)

* Definimos un rayo 

$$\mathbf{r}(s)=\mathbf{r}_0+\mathbf{v}s.$$

* Queremos escribir la intensidad en función de $s$ sobre el rayo.
* Hay dos puntos de referencia inicial y final $s=a$, $s=b$.
* Conocemos el coeficiente de atenuación $\mu(\mathbf{x})$.
* Discutir.

## Atenuación de una fuente puntual (planilandia)

* Una fuente puntual en el plano
* La intensidad decae como $1/r$. Recordar ley de Gauss.
* Se puede asignar un $\mu$ a este efecto.
    * ¿Cómo? ¿Cuánto vale $\mu$?
* Cuando el efecto de dispersión del haz no es relevante en la aplicación, tenemos un haz \mathbf{no divergente}.
    * ¿Cuándo ocurre esto?

## Ley de Beer y sombras de objetos

* Usando la Ley de Beer se puede calcular la función de sombra de objetos.
* Es un problema directo completo.
* Discutir.

## Transformación de una imagen bidimensional

* Phantom matemático: Shepp-Logan
    * 10 elipses en un cuadrado (-1,-1) - (1,1)


| Ellipse | Center | Major Axis | Minor Axis | Theta | Gray Level |
| --- | --- | --- | --- | --- | --- |
| a | (0,0) | 0.69 | 0.92 | 0 | 2 |
| b | (0,−0.0184) | 0.6624 | 0.874 | 0 | −0.98 |
| c | (0.22,0) | 0.11 | 0.31 | −18° | −0.02 |
| d | (−0.22,0) | 0.16 | 0.41 | 18° | −0.02 |
| e | (0,0.35) | 0.21 | 0.25 | 0 | 0.01 |
| f | (0,0.1) | 0.046 | 0.046 | 0 | 0.01 |
| g | (0,−0.1) | 0.046 | 0.046 | 0 | 0.01 |
| h | (−0.08,−0.605) | 0.046 | 0.023 | 0 | 0.01 |
| i | (0,−0.605) | 0.023 | 0.023 | 0 | 0.01 |
| j | (0.06,−0.605) | 0.023 | 0.046 | 0 | 0.01 |

## Shepp-Logan
* Imagen

* Tralarilari, tralarilala

## Dar la vuelta

* Incluir imagen

* Discutir ¿Cómo queda el phantom?

## Transformada de Radon

* Recordemos el espacio de los rayos