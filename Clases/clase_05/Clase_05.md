# Clase 5: Transformada de Fourier
## Introducción

* Fundamental en el análisis de gran cantidad de propiedades de funciones.
* Aplicaciones en ecuaciones diferenciales.
* Descomposición de una función en sus componentes armónicas.
* Instrumentación: En general la interacción de un instrumento con un sistema depende de la frecuencia.

## Función exponencial compleja I
* Recordemos la fórmula de Euler

$$e^{i\theta} = \cos\theta + i\sin\theta$$

* ¿Cómo se obtiene esta expresión?
* Expresión polar de un número complejo

$$z=e^{s+i\theta},$$

$$s=\ln|z|.$$

* De modo que

$$\ln z = \ln|z| + i\arctan(\frac{\mathcal{Im}~z}{\mathcal{Re}~z}).$$

* El logaritmo es \textit{multivaluado}.

## Función exponencial compleja II
* La exponencial compleja copia propiedades conocidas. Se pueden demostrar usando la expresión en serie potencias.

$$\int_0^x dy~e^{zy} = \frac{e^{zx}-1}{z},$$

$$\partial_x~e^{izx}=ize^{izx}.$$

## Transformada de Fourier
* Para una variable
* Funciones $L^1$ (absolutamente integrables)

$$||f||_1\int_{\mathcal{R}^n}~|f(\mathbf{x})|<\infty .$$

* La norma $||~\cdot~||_1$ define una distancia.
* Las $f(\mathbf{x}) \in L^1$ son un espacio vectorial, etc.
* Las $f(\mathbf{x})$ son clases de equivalencia de funciones que difieren en un conjunto de medida nula.
* Definimos la transformada de Fourier de una $f(\mathbf{x}) \in L^1$,

$$\mathcal{F}(f)=\hat{f}(\xi)=\int_{-\infty}^{\infty}dx~f(x)e^{-ix\xi}.$$

## Inversa de la Transformada de Fourier

* La función original es reconstruible a partir de la transformada,

$$f(x)=\mathcal{F}^{-1}(\hat{f})=\frac{1}{2\pi}\int_{-\infty}^{\infty}d\xi~\hat{f}(\xi)e^{ix\xi}.$$

* Recordamos
$$f(-x)=\mathcal{F}(\mathcal{F}(f))(x).$$


## Algunas Transformadas
* Recordemos la función carcterística,

$$
\chi_D=\left\{ \begin{array}{cc} 1 & x\in D \\ 0 & x\notin D \end{array} \right.
$$

* Si $D$ es el intervalo $[a,b)$:

$$\chi_{[a,b)}(x)=1,~~x\in [a,b),$$
$$\chi_{[a,b)}(x)=0,~~x\notin [a,b)$$

* Transformada 

$$\hat{\chi}_{[a,b)}(\xi) = \frac{e^{-ia\xi}-e^{-ib\xi}}{i\xi}$$

* Definimos $r_1(x)=\chi_{[-1,1)}(x)$. La transformada es la función sinc($\xi$)

$$\hat{r}_1(\xi)=\frac{\sin \xi}{\xi}.$$

* $\hat{r}_1(\xi)$ no es $L^1$ :(


## Regularidad y decaimiento de la transformada

* Si $f$ es continua, la transformada decae para altas frecuencias ($|\xi| \longrightarrow \infty$).

* Lema de Riemann-Lebesgue. Si $f \in L^1$, su transformada es contínua y decae para altas frecuencias. Recordar el caso $\chi_{[a,b)}(x)$.

* Regularidad y diferenciabilidad. Mientras más derivadas continuas, más regular. $\mathcal{C}^n(\mathcal{R})$ es el conjunto de funciones en $\mathcal{R}$ con $n$ derivadas continuas.

* Supongamos que ambas $f$ y $f'$ son $L^1$,

$$\hat{f}(\xi) = \frac{\widehat{f'}(\xi)}{i\xi}.$$

* $\widehat{f'}(\xi)$ tiende a cero para $\xi$ grande, más rápido que $1/\xi$.

## Más regularidad
* Si $f$ es $L^1$ tiene $n$ derivadas también $L^1$,

$$|\hat{f}(\xi)| \leq \frac{C}{(1+|\xi|)^n}.$$

* La transformada de Fourier de las derivadas es

$$\widehat{f^{(n)}}(\xi) = (i\xi)^n\hat{f}(\xi)$$

## Fórmula de Parseval
* Vamos al conjunto de las funciones de (módulo) cuadrado integrable $L^2$

$$||f||_{L^2}^2=\int_{\mathcal{R}^n}dx^n~|f(\mathbf{x})|^2~<~\infty.$$

* La norma en $L^2$ se define a partir del producto escalar,

$$\langle f,g\rangle_{L^2}=\int_{\mathcal{R}^n}dx^n~f(\mathbf{x})\overline{g(\mathbf{x})}.$$


* Teorema: Fórmula de Parseval
    * Si $f$ es $L^2$,

    $$\int_{-\infty}^{\infty}dx~|f(x)|^2=\int_{-\infty}^{\infty}\frac{d\xi}{2\pi}~|\hat{f}(\xi)|^2$$

## Propiedades de la Transformada de Fourier en $L^2$

* Linealidad 
$$\widehat{f+g}=\hat{f}+\hat{g}$$
* Escalamiento 
$$\widehat{f(ax)}(\xi)=\frac{1}{a}\hat{f}(\frac{\xi}{a})$$
* Traslación 
$$\widehat{f(x-t)}(\xi)=e^{-it\xi}\hat{f}(\xi)$$
* Realidad. Si $f$ es real, 
$$\hat{f}(\xi)=\overline{\hat{f}(-\xi)}.$$
* Paridad. Si $f$ es real y par, $\hat{f}$ es real. Si $f$ es real e impar, $\hat{f}$ es imaginaria.

## Derivadas débiles

* Las medidas reales son un promedio

$$m_g(f)=\int_{-\infty}^{\infty}dx~f(X)g(x).$$

* $g$ es una función de prueba.

* $m_g(f)$ es la acción de $f$ sobre la función de prueba $g$

Se cumple (y sirve como generalización de la definición de derivada)

$$m_g(f')=m_{g'}(f).$$


## Tarea

* Ejercicios 2.4.5 y 4.3.3