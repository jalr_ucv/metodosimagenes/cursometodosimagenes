\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}
\usepackage{pgfplots}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 5: Transformada de Fourier}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{31 de agosto de 2023} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Introducción: Transformada de Fourier}
\begin{itemize}
  \item Fundamental en el análisis de gran cantidad de propiedades de funciones.
  \item Aplicaciones en ecuaciones diferenciales.
  \item Descomposición de una función en sus componentes armónicas.
  \item Instrumentación: En general la interacción de un instrumento con un sistema depende de la frecuencia.
\end{itemize}
\end{frame}

\begin{frame}{Función exponencial compleja I}
\begin{itemize}
  \item Recordemos la fórmula de Euler
 $$e^{i\theta} = \cos\theta + i\sin\theta$$
  \item ¿Cómo se obtiene esta expresión?
  \item Expresión polar de un número complejo
 $$z=e^{s+i\theta},$$
 $$s=\ln|z|.$$
  \item De modo que
 $$\ln z = \ln|z| + i\arctan(\frac{{Im}~z}{{Re}~z}).$$
  \item El logaritmo es \textit{multivaluado}.
\end{itemize}
\end{frame}

\begin{frame}{Función exponencial compleja II}
\begin{itemize}
  \item La exponencial compleja copia propiedades conocidas. Se pueden demostrar usando la expresión en serie potencias.
  \item $$\int_0^x dy~e^{zy} = \frac{e^{zx}-1}{z},$$
  \item $$\frac{d}{dx}~e^{izx}=ize^{izx}.$$
\end{itemize}
\end{frame}

\begin{frame}{Transformada de Fourier}
\begin{itemize}
  \item Para una variable
  \item Funciones $L^1$ (absolutamente integrables)
$$||f||_1=\int_{\mathcal{R}^n}~|f(\mathbf{x})|<\infty .$$
  \item La norma $||~\cdot~||_1$ define una distancia.
  \item Las $f(\mathbf{x}) \in L^1$ son un espacio vectorial, etc.
  \item Las $f(\mathbf{x})$ son clases de equivalencia de funciones que difieren en un conjunto de medida nula.
  \item Definimos la transformada de Fourier de una $f(\mathbf{x}) \in L^1$,
$$\mathcal{F}(f)=\hat{f}(\xi)=\int_{-\infty}^{\infty}dx~f(x)e^{-ix\xi}.$$
\end{itemize}
\end{frame}

\begin{frame}{Inversa de la Transformada de Fourier}
\begin{itemize}
  \item La función original es reconstruible a partir de la transformada,
 $$f(x)=\mathcal{F}^{-1}(\hat{f})=\frac{1}{2\pi}\int_{-\infty}^{\infty}d\xi~\hat{f}(\xi)e^{ix\xi}.$$
  \item Recordamos
 $$2\pi f(-x)=\mathcal{F}(\mathcal{F}(f))(x).$$
\end{itemize}
\end{frame}

\begin{frame}{Algunas Transformadas}
\begin{itemize}
  \item Recordemos la función carcterística,
 $$
    \chi_D=\left\{ \begin{array}{cc} 1 & x\in D \\ 0 & x\notin D \end{array} \right.
    $$
  \item Si $D$ es el intervalo $[a,b)$:
 $$\chi_{[a,b)}(x)=1,~~x\in [a,b),$$
 $$\chi_{[a,b)}(x)=0,~~x\notin [a,b)$$
  \item Transformada 
 $$\hat{\chi}_{[a,b)}(\xi) = \frac{e^{-ia\xi}-e^{-ib\xi}}{i\xi}$$
  \item Definimos $r_1(x)=\chi_{[-1,1)}(x)$. La transformada es la función sinc($\xi$)
 $$\hat{r}_1(\xi)=2\frac{\sin \xi}{\xi}.$$
  \item $\hat{r}_1(\xi)$ no es $L^1$ :(
\end{itemize}
\end{frame}

\begin{frame}{Regularidad y decaimiento de la transformada}
\begin{itemize}
  \item Si $f$ es continua, la transformada decae para altas frecuencias ($|\xi| \longrightarrow \infty$).
  \item Lema de Riemann-Lebesgue. Si $f \in L^1$, su transformada es contínua y decae para altas frecuencias. Recordar el caso $\chi_{[a,b)}(x)$.
  \item Regularidad y diferenciabilidad. Mientras más derivadas continuas, más regular. $\mathcal{C}^n(\mathcal{R})$ es el conjunto de funciones en $\mathcal{R}$ con $n$ derivadas continuas.
  \item Supongamos que ambas $f$ y $f'$ son $L^1$,
 $$\hat{f}(\xi) = \frac{\widehat{f'}(\xi)}{i\xi}.$$
  \item $\widehat{f'}(\xi)$ tiende a cero para $\xi$ grande, más rápido que $1/\xi$.
\end{itemize}
\end{frame}

\begin{frame}{Más regularidad}
\begin{itemize}
  \item Si $f$ es $L^1$ tiene $n$ derivadas también $L^1$,
$$|\hat{f}(\xi)| \leq \frac{C}{(1+|\xi|)^n}.$$
  \item La transformada de Fourier de las derivadas es
$$\widehat{f^{(n)}}(\xi) = (i\xi)^n\hat{f}(\xi)$$
\end{itemize}
\end{frame}

\begin{frame}{Fórmula de Parseval}
\begin{itemize}
  \item Vamos al conjunto de las funciones de (módulo) cuadrado integrable $L^2$
$$||f||_{L^2}^2=\int_{\mathcal{R}^n}dx^n~|f(\mathbf{x})|^2~<~\infty.$$
  \item La norma en $L^2$ se define a partir del producto escalar,
 $$\langle f,g\rangle_{L^2}=\int_{\mathcal{R}^n}dx^n~f(\mathbf{x})\overline{g(\mathbf{x})}.$$
  \item Teorema: Fórmula de Parseval
    \item Si $f$ es $L^2$,
    $$\int_{-\infty}^{\infty}dx~|f(x)|^2=\int_{-\infty}^{\infty}\frac{d\xi}{2\pi}~|\hat{f}(\xi)|^2$$
\end{itemize}
\end{frame}

\begin{frame}{Propiedades de la Transformada de Fourier en $L^2$}
\begin{itemize}
  \item Linealidad 
  $$\widehat{f+g}=\hat{f}+\hat{g}$$
  \item Escalamiento 
  $$\widehat{f(ax)}(\xi)=\frac{1}{a}\hat{f}(\frac{\xi}{a})$$
  \item Traslación 
  $$\widehat{f(x-t)}(\xi)=e^{-it\xi}\hat{f}(\xi)$$
  \item Realidad. Si $f$ es real, 
  $$\hat{f}(\xi)=\overline{\hat{f}(-\xi)}.$$
  \item Paridad. Si $f$ es real y par, $\hat{f}$ es real. Si $f$ es real e impar, $\hat{f}$ es imaginaria.
\end{itemize}
\end{frame}

\begin{frame}{Derivadas débiles}
\begin{itemize}
  \item Las medidas reales son un promedio
  $$m_g(f)=\int_{-\infty}^{\infty}dx~f(x)g(x).$$
  \item $g$ es una función de prueba.
  \item $m_g(f)$ es la acción de $f$ sobre la función de prueba $g$
  Se cumple (y sirve como generalización de la definición de derivada)
  $$m_g(f')=-m_{g'}(f).$$
\end{itemize}
\end{frame}

\begin{frame}{Tarea}
\begin{itemize}
  \item Ejercicios 4.2.5 y 4.3.3
\end{itemize}
\end{frame}

\end{document}
