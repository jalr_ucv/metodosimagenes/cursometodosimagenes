\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}
\usepackage{pgfplots}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 7: Transformada de Radon y Retroproyección filtrada}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{14 de septiembre de 2023} % Date, can be changed to a custom date

\begin{document}

\frame{\titlepage}

\begin{frame}{Introducción}
\begin{itemize}
    \item Objetivos:
    \begin{itemize}
        \item Relación entre Transformada de Radon y Transformada de Fourier.
        \item Retroproyección filtrada: la inversa exacta de la transformada de Radon.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Espacio de líneas orientadas}
\begin{itemize}
    \item Recordemos el espacio de las líneas orientadas en el plano.
    \begin{itemize}
        \item Se parametriza usando una recta y un círculo $(t,\vec{\omega})$.
        \item $t$ es un número real (parámetro afín).
        \item $\vec{\omega}$ es un vector unitario.
        \item La línea orientada $l_{t,\vec{\omega}}$ es perpendicular a la dirección $\vec{\omega}$ y pasa a distancia $t$ del origen.
        \item Definimos $\mathbf{\hat{\omega}}$. Un vector perpendicular a $\vec{\omega}$ en la dirección en que aumenta el ángulo de $\vec{\omega}$ con el eje $x$.
        \item Esta condición se puede escribir así $\det{(\vec{\omega}|\mathbf{\hat{\omega}}})>0$. Es una matriz cuyas columnas son los vectores $\vec{\omega}$ y $\mathbf{\hat{\omega}}$.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Transformada de Radon}
\begin{itemize}
    \item Recordemos la transformada de Radon
    $$\mathcal{R}f(t,\vec{\omega})= \int_{-\infty}^{\infty}f(s\hat{\omega}+t{\vec{\omega}})ds$$
    \item Se cumple
    $$\mathcal{R}(f\star g)(t,\vec{\omega})=\int_{-\infty}^{\infty}\mathcal{R}f(s,\vec{\omega})\mathcal{R}g(t-s,\vec{\omega})~ds.$$
    \item La transformada de Radon convierte una convolución en el plano (2D) en una convolución en el parámetro afín (1D).
\end{itemize}
\end{frame}

\begin{frame}{Traslaciones y rotaciones}
\begin{itemize}
    \item En una traslación $f_{\mathbf{v}}(\mathbf{x})\equiv f(\mathbf{x}-\mathbf{v})$
    $$\mathcal{R}f_{\mathbf{v}}(t,\vec{\omega})=\mathcal{R}f(t-\vec{\omega}\cdot\mathbf{v},\vec{\omega}).$$
    \item Se cumple
    $$\mathcal{R}(\nabla_{\mathbf{x}} f)(t,\vec{\omega})=\vec{\omega}\partial_t \mathcal{R}f(t,\vec{\omega}).$$
    \item En una rotación de las variables del plano $f_A(\mathbf{x})\equiv f(A\mathbf{x})$
    $$\mathcal{R}f_{A}(t,\vec{\omega})=\mathcal{R}f(t,A\vec{\omega}).$$
\end{itemize}
\end{frame}

\begin{frame}{Teorema de la lámina central (\textit{Central Slice Theorem})}
\begin{itemize}
    \item Sea $f(\mathbf{x})$ una función adecuada
    $$\int_{-\infty}^{\infty}\mathcal{R}f(t,\vec{\omega})~e^{-itr}~dt = \hat{f}(r\vec{\omega}).$$
    \item La transformada de Fourier en $t$ (1D) es la transformada de Fourier 2D de la función $f$
    \item Definimos la transformada de Fourier 1D en el parámetro afín.
    $$\tilde{h}(r,\vec{\omega})\equiv \int_{-\infty}^{\infty} h(t,\vec{\omega})e^{-irt}~dt$$
    \item Tenemos
    $$\widetilde{\mathcal{R}f}(r,\vec{\omega}) = \int_{-\infty}^{\infty} {\mathcal{R}f}(t,\vec{\omega})e^{-irt}~dt = \hat{f}(r\vec{\omega})$$
\end{itemize}
\end{frame}

\begin{frame}{Inversión de la transformación de Radon}
\begin{itemize}
    \item Debemos invertir la función $\hat{f}(r\vec{\omega})$,
    $$f(\mathbf{x}) = \frac{1}{(2\pi)^2}\int_{0}^{\pi} \int_{-\infty}^{\infty} e^{ir\mathbf{x}\cdot\vec{\omega}} \widetilde{\mathcal{R}f}(r,\vec{\omega})|r|drd\omega.$$
    \item Discutir
\end{itemize}
\end{frame}

\begin{frame}{Esquema general de inversión}
\begin{itemize}
    \item El problema es obtener el coeficiente de atenuación con respecto a la posición en la \textit{slice}. Esa es nuestra función $f(\mathbf{x})$.
    \item Los rayos que se desplazan a través de una linea $l_{(t,\vec{\omega})}$ sufren una atenuación que responde a esta relación,
    $$\frac{dI_{(t,\vec{\omega})}(s)}{ds}=-fI_{(t,\vec{\omega})}(s).$$
    \item La solución de $I_{(t,\vec{\omega})}$ lleva directamente a la transformada de Radon de la función de atenuación $f$.
    $$\mathcal{R}f(t,\vec{\omega})=-\log\left[ \frac{I_{output,~(t,\vec{\omega})}}{I_{input,~(t,\vec{\omega})}} \right].$$
    \item El lado derecho se mide en el tomógrafo.
    \item $f$ se reconstruye a partir de la transformada de Fourier 2D inversa de la transformada de Fourier 1D afín del logarirmo de la intensidad relativa.
\end{itemize}
\end{frame}

\begin{frame}{Retroproyección filtrada (\textit{Filtered Back Projection})}
\begin{itemize}
    \item Es común indicar la inversión de la transformada de Radon como la combinación de un filtro radial
    $$\mathcal{GR}f(t,\vec\omega)=\frac{1}{2\pi}\int_{-\infty}^{\infty} e^{ir\mathbf{x}\cdot\vec{\omega}} \widetilde{\mathcal{R}f}(r,\vec{\omega})|r|dr.$$
    \item y la retroproyección, que es la integral angular
    $$f(\mathbf x) = \frac{1}{2\pi}\int_{0}^{\pi}\mathcal{GR}f(\mathbf x\cdot\vec\omega ,\vec\omega)d\omega$$
\end{itemize}
\end{frame}

\begin{frame}{Tarea}
\begin{itemize}
    \item Demostrar analíticamente la relación entre la función de atenuación y las intensidades del haz de rayos X.
\end{itemize}
\end{frame}

\end{document}
