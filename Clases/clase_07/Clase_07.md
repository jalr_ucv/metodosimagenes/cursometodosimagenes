# Clase 7: Transformada de Radon y Retroproyección filtrada
## Introducción

* Objetivos:
    * Relación entre Transforamda de Radon y Transformada de Fourier.
    * Retroproyección filtrada: el la inversa exacta de la transformada de Radon.

## Espacio lineas orientadas
* Recordemos el espacio de las lineas orientadas en el plano.
    * Se parametriza usando una recta y un círculo $(t,\vec{\omega})$.
    * $t$ es un número real (parámetro afín).
    * $\vec{\omega}$ es un vector unitario.
    * La linea orientada $l_{t,\vec{\omega}}$ es perpendicular a la dirección $\vec{\omega}$ y pasa a distancia $t$ del origen.
    * Definimos $\mathbf{\hat{\omega}}$. Un vector perpendicular a $\vec{\omega}$ en la dirección en que amenta el ángulo de $\vec{\omega}$ con el eje $x$.
    * Esta condición se puede escribir así $\det{(\vec{\omega}|\mathbf{\hat{\omega}}})>0$. Es una matriz cuyas columnas son los vectores $\vec{\omega}$ y $\mathbf{\hat{\omega}}$.

$$l_{t,\vec{\omega}}(s)=t\vec{\omega} + s\mathbf{\hat\omega}.$$


## Transformada de Radon

* Recordemos la transformada de Radon

$$\mathcal{R}f(t,\vec{\omega})= \int_{-\infty}^{\infty}f(s\hat{\omega}+t{\vec{\omega}})ds$$

* Se cumple

$$\mathcal{R}(f\star g)(t,\vec{\omega})=\int_{-\infty}^{\infty}\mathcal{R}f(s,\vec{\omega})\mathcal{R}g(t-s,\vec{\omega})~ds.$$

* La transformada de Radon convierte una convolución en el plano (2D) en una convolución en el parámetro afín (1D).



## Traslaciones y rotaciones

* En una traslación $f_{\mathbf{v}}(\mathbf{x})\equiv f(\mathbf{x}-\mathbf{v})$

$$\mathcal{R}f_{\mathbf{v}}(t,\vec{\omega})=\mathcal{R}f(t-\vec{\omega}\cdot\mathbf{v},\vec{\omega}).$$

* Se cumple

$$\mathcal{R}(\nabla_{\mathbf{x}} f)(t,\vec{\omega})=\vec{\omega}\partial_t \mathcal{R}f(t,\vec{\omega}).$$

* En una rotación de las variables del plano $f_A(\mathbf{x})\equiv f(A\mathbf{x})$

$$\mathcal{R}f_{A}(t,\vec{\omega})=\mathcal{R}f(t,A\vec{\omega}).$$

## Teorema de la lámina central (\textit{Central Slice Theorem})

* Sea $f(\mathbf{x})$ una función adecuada

$$\int_{-\infty}^{\infty}\mathcal{R}f(t,\vec{\omega})~e^{-itr}~dt = \hat{f}(r\vec{\omega}).$$

* La transformada de Fourier en $t$ (1D) es la transformada de Fourier 2D de la función $f$

* Definimos la transformada de Fourier 1D en el parámetro afín.

$$\tilde{h}(r,\vec{\omega})\equiv \int_{-\infty}^{\infty} h(t,\vec{\omega})e^{-irt}~dt$$

* Tenemos

$$\widetilde{\mathcal{{R}f}}(r,\vec{\omega}) = \int_{-\infty}^{\infty} {\mathcal{{R}f}}(t,\vec{\omega})e^{-irt}~dt = \hat{f}(r\vec{\omega})$$

## Inversión de la transformación de Radon

* Debemos invertir la función $\hat{f}(r\vec{\omega})$,

$$f(\mathbf{x}) = \frac{1}{(2\pi)^2}\int_{0}^{\pi} \int_{-\infty}^{\infty} e^{ir\mathbf{x}\cdot\vec{\omega}} \widetilde{\mathcal{Rf}}(r,\vec{\omega})|r|drd\omega.$$

* Discutir

## Esquema general de inversión

* El problema es obtener el coeficiente de atenuación con respecto a la posición en la \textit{slice}. Esa es nuestra función $f(\mathbf{x})$.

* Los rayos que se desplazan a través de una linea $l_{(t,\vec{\omega})}$ sufren una atenuación que responde a esta relación,

$$\frac{dI_{(t,\vec{\omega})}(s)}{ds}=-fI_{(t,\vec{\omega})}(s).$$

* La solución de $I_{(t,\vec{\omega})}$ lleva directamente a la transformada de Radon de la función de atenuación $f$.

$$\mathcal{R}f(t,\vec{\omega})=-\log\left[ \frac{I_{output,~(t,\vec{\omega})}}{I_{input,~(t,\vec{\omega})}} \right].$$

* El lado derecho se mide en el tomógrafo.

* $f$ se reconstruye a partir de la transformada de Fourier 2D inversa de la transformada de Fourier 1D afín del logarirmo de la intensidad relativa.

## Retroproyeccion filtrada (\textit{Filtered Back Projection})

* Es común indicar la inversión de la transformada de Radon como la combinación de un filtro radial

$$\mathcal{GR}f(t,\vec\omega)=\frac{1}{2\pi}\int_{-\infty}^{\infty} e^{ir\mathbf{x}\cdot\vec{\omega}} \widetilde{\mathcal{Rf}}(r,\vec{\omega})|r|dr.$$

* y la retroproyección, que es la integral angular

$$f(\mathbf x) = \frac{1}{2\pi}\int_{0}^{\pi}\mathcal{GR}f(\mathbf x\cdot\vec\omega ,\vec\omega)d\omega$$

## Tarea

* Demostrar analíticamente la relación entre la función de atenuación y las intensidades del haz de rayos X.