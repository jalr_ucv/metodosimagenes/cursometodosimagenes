# Clase 3: Modelos lineales y ecuaciones lineales
## Introducción

* Hacemos mediciones
* Problema directo 
    * La teoría hace predicciones 
    * Input \checkmark + sistema \checkmark $\rightarrow$ Output?
    * Las medidas sobre el sistema ponen a prueba la teoría
* Problema inverso
    * Queremos sacar información de las medidas
    * Input \checkmark + sistema? $\rightarrow$ Output \checkmark
    * Sistemas de ecuaciones determinan los parámetros del sistema
    * Complicaciones típicamente aparecen al "invertir la teoría"
    * Los sistemas de ecuaciones lineales son frecuentes en las técnicas de inversión.

### Sistemas lineales
* Los sistemas de ecuaciones lineales están emparentados con las transformaciones lineales.

$$ \mathbf{Ax}=\mathbf{y}.$$

* $ \mathbf{x}$ es un vector en $\mathcal{R}^n$.
* $ \mathbf{y}$ es un vector en $\mathcal{R}^m$.
* $ \mathbf{A}$ es una matriz $m\times n$.
* En lenguaje matricial $\mathbf{x}$ y $\mathbf{y}$ son matrices $n\times 1$ y $m\times 1$ respectivamente.
* Interprtaciones
    * $ \mathbf{A}$ es una transformación lineal de $\mathcal{R}^n \rightarrow \mathcal{R}^n$. Es una versión de un problema directo.
    * Conocido el valor de $ \mathbf{y}$, queremos conocer el valor de $ \mathbf{x}$. Es una versión de un problema inverso.

#### Algunas propiedades de los sistemas lineales
* ¿Cuáles son?
    * ¿Suma en los $ \mathbf{x}$?
    * ¿Producto por escalares en los $ \mathbf{x}$?

#### Matriz de medición
* Tomar una fila arbitraria ($i$) de $\mathbf{A}$.
    * El valor $y_i$ puede ser interpretado como la $i$-ésima medida sobre el sistema en el estado $\mathbf{x}$

    $${\mathbf{A}_{i}}^{j}x_j = \langle \mathbf{A}_{i}, \mathbf{x} \rangle = y_i.$$

* Discutir el significado. Recordar mínimos cuadrados.
    * $\mathbf{x}$ es el estado del sistema (a determinar)
    * $\mathbf{A}_{i}$ es la $i$-ésima medición.
    * $y_{i}$ es el resultado de la $i$-ésima medición.
    * Problema: determinar el valor de la velocidad y la posición incial de un móvil conocidas las posiciónes para dos tiempos particulares.
* Si repetimos una medida $\mathbf{A}_{i}$ y $\mathbf{A}_{k}$, se repite una fila de la matriz de medición $\mathbf{A}_{i}=\mathbf{A}_{k}$.
    * La incertidumbre de las medidas garantiza que muy probablemente no se repita la salida $y_i \neq y_k$.
    * ¿Consecuencias?

#### Espacio columna
* Definimos las columnas de $\mathbf{A}$
    * $\mathbf{A}^j$
    * $ \mathbf{y}$ es combinación lineal de las columnas de $\mathbf{A}$.
    * $ \mathbf{y} = x_j\mathbf{A}^j$.
    * Espacio columna. Espacio imagen.

#### Tres preguntas

* Existencia. ¿Dado un $ \mathbf{y}$, existe un $ \mathbf{x}$?
    * Ver primero Unicidad, luego discutir la condición sobre el espacio nulo de $\mathbf{A}^\dagger$.
* Unicidad. Si la solución existe, ¿es única?
    * Discutir espacio nulo de $\mathbf{A}$.
* Estabilidad. ¿Cómo cambia la solución ante variaciones en la medición ($\mathbf{A}$) o en las medidas ($ \mathbf{y}$)?

### Tres tipos de sistemas lineales
* Determinados
    * La solución existe y es única.
* Sobredeterminados
    * Hay más medidas que parámetros a determinar. 
    * $\mathbf{A}$ tiene más filas que columnas.
    * La incertidumbre de las medidas implica que el sistema no tendrá solución.
* Infradeterminados
    * No hay suficientes ecuaciones para hayar la solución.
    * $\mathbf{A}$ tiene más columnas que filas.

#### Estabilidad
* Norma de una matriz
$$||\mathbf{A}|| = max\left\{ ||\mathbf{A}\mathbf{x}||: \mathbf{x} \in \mathcal{R}^n ~~y~~||\mathbf{x}||=1 \right\}.$$
* Propiedades interesantes
    * $||c\mathbf{A}||$ = $|c|~||\mathbf{A}||$
    * $||\mathbf{A}_1 \pm \mathbf{A}_2|| \leq ||\mathbf{A}_1|| + ||\mathbf{A}_2||$
    * $||\mathbf{A}\mathbf{x}|| \leq ||\mathbf{A}|| ~||\mathbf{x}||$

#### Acotación y propagación de errores
* Suponemos un sistema determinado

$$\mathbf{A}\mathbf{x} = \mathbf{y}.$$

* Queremos acotar el error en las soluciones $\delta\mathbf{x}$ a partir de los datos y los errores en las medidas $\delta\mathbf{A}$ y en las salidas del sistema $\delta\mathbf{y}$.

* Se obtiene

$$\frac{||\delta\mathbf{x}||}{||\mathbf{x}||} \leq c_a \left[ \frac{||\delta\mathbf{y}||}{||\mathbf{x}||} + \frac{||\delta\mathbf{A}||}{||\mathbf{A}||}\right],$$

* donde $c_a \equiv ||\mathbf{A}||~||\mathbf{A}^{-1}||$.

### Sobre los espacios de dimensión infinita

* Ejemplos de espacios de dimensión infinita
    * Los polinomios
    * Las funciones continuas de $\mathcal{R} \longrightarrow \mathcal{R}$.
    * Las funciones de $[0,1] \longrightarrow \mathcal{R}$, cuyo valor absoluto es integrable $L^1[0,1]$.
* En casos concretos, como $L^1[0,1]$, se puede definir una norma.

$$||f|| = \int_0^1 dx |f(x)|.$$

#### Operadores lineales
* Los valores de una función vistos como coordenadas de un vector.
* Un operador lineal

$$\mathcal{M}f(x) = \int dy ~m(x,y)f(y).$$

* Es un operador integral. Discutir.

#### La integral indefinida

* La integral indefinida es un buen operador lineal sobre las funciones continuas

$$\mathcal{G}(f)(x) = \int_0^x dy ~f(y).$$

* Discutir las propiedades del operador inversa por la izquierda (el operador derivada),

$$\frac{d}{dx}.$$

* ¿Por qué solo inversa izquierda?
* ¿Recordamos métodos? Funciones de Green, convoluciones, etc.

### Números complejos

* Discusión: Hacer la lista de las propiedades interesantes.
* ¿Qué se puede agregar interesante en los espacios vectoriales complejos?