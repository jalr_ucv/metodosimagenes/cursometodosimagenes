\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 3: Modelos lineales y ecuaciones lineales}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{17 de agosto de 2023} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Introducción}
\begin{itemize}
  \item Hacemos mediciones
  \item Problema directo
    \begin{itemize}
      \item La teoría hace predicciones.
      \item Input (\checkmark) + sistema (\checkmark) $\rightarrow$ Output (?).
      \item Las medidas sobre el sistema ponen a prueba la teoría.
    \end{itemize}
  \item Problema inverso
    \begin{itemize}
      \item Queremos sacar información de las medidas.
      \item Input (\checkmark) + sistema (?) $\rightarrow$ Output (\checkmark).
      \item Sistemas de ecuaciones determinan los parámetros del sistema.
      \item Complicaciones típicamente aparecen al ``invertir la teoría''.
      \item Los sistemas de ecuaciones lineales son frecuentes en las técnicas de inversión.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Sistemas lineales}
\begin{itemize}
  \item Los sistemas de ecuaciones lineales están emparentados con las transformaciones lineales.
  \item $$ \mathbf{Ax}=\mathbf{y}.$$
  \item $ \mathbf{x}$ es un vector en $\mathcal{R}^n$.
  \item $ \mathbf{y}$ es un vector en $\mathcal{R}^m$.
  \item $ \mathbf{A}$ es una matriz $m\times n$.
  \item En lenguaje matricial $\mathbf{x}$ y $\mathbf{y}$ son matrices $n\times 1$ y $m\times 1$ respectivamente.
  \item Interpretaciones
    \begin{itemize}
      \item $ \mathbf{A}$ es una transformación lineal de $\mathcal{R}^n \rightarrow \mathcal{R}^m$. Es una versión de un problema directo.
      \item Conocido el valor de $ \mathbf{y}$, queremos conocer el valor de $ \mathbf{x}$. Es una versión de un problema inverso.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Algunas propiedades de los sistemas lineales}
\begin{itemize}
  \item ¿Cuáles son?
    \begin{itemize}
      \item ¿Suma en los $ \mathbf{x}$?
      \item ¿Producto por escalares en los $ \mathbf{x}$?
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Matriz de medición}
\begin{itemize}
  \item Tomar una fila arbitraria ($i$) de $\mathbf{A}$.
  \item El valor $y_i$ puede ser interpretado como la $i$-ésima medida sobre el sistema en el estado $\mathbf{x}$.
  \item $${\mathbf{A}_{i}}^{j}x_j = \langle \mathbf{A}_{i}, \mathbf{x} \rangle = y_i.$$
  \item Discutir el significado. Recordar mínimos cuadrados.
    \begin{itemize}
      \item $\mathbf{x}$ es el estado del sistema (a determinar).
      \item $\mathbf{A}_{i}$ es la $i$-ésima medición.
      \item $y_{i}$ es el resultado de la $i$-ésima medición.
      \item Problema: determinar el valor de la velocidad y la posición inicial de un móvil conocidas las posiciones para dos tiempos particulares.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Matriz de medición}
\begin{itemize}
  \item Si repetimos una medida $\mathbf{A}_{i}$ y $\mathbf{A}_{k}$, se repite una fila de la matriz de medición $\mathbf{A}_{i}=\mathbf{A}_{k}$.
  \item La incertidumbre de las medidas garantiza que muy probablemente no se repita la salida $y_i \neq y_k$.
  \item ¿Consecuencias?
\end{itemize}
\end{frame}

\begin{frame}{Espacio columna}
\begin{itemize}
  \item Definimos las columnas de $\mathbf{A}$
    \begin{itemize}
      \item $\mathbf{A}^j$
      \item $ \mathbf{y}$ es combinación lineal de las columnas de $\mathbf{A}$.
      \item $ \mathbf{y} = x_j\mathbf{A}^j$.
      \item Espacio columna. Espacio imagen.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Tres preguntas}
\begin{itemize}
  \item Existencia. ¿Dado un $ \mathbf{y}$, existe un $ \mathbf{x}$?
    \begin{itemize}
      \item Ver primero Unicidad, luego discutir la condición sobre el espacio nulo de $\mathbf{A}^\dagger$.
    \end{itemize}
  \item Unicidad. Si la solución existe, ¿es única?
    \begin{itemize}
      \item Discutir espacio nulo de $\mathbf{A}$.
    \end{itemize}
  \item Estabilidad. ¿Cómo cambia la solución ante variaciones en la medición ($\mathbf{A}$) o en las medidas ($ \mathbf{y}$)?
\end{itemize}
\end{frame}

\begin{frame}{Tipos de sistemas}
\begin{itemize}
  \item Determinados.
    \begin{itemize}
      \item La solución existe y es única.
    \end{itemize}
  \item Sobredeterminados.
    \begin{itemize}
      \item Hay más medidas que parámetros a determinar. 
	  \item $\mathbf{A}$ tiene más filas que columnas.
	  \item La incertidumbre de las medidas implica que el sistema no tendrá solución.
    \end{itemize}
  \item Infradeterminados.
  \begin{itemize}
      \item No hay suficientes ecuaciones para hayar la solución. 
	  \item $\mathbf{A}$ tiene más columnas que filas.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Estabilidad}
\begin{itemize}
  \item Norma de una matriz
  \item $$||\mathbf{A}|| = max\left\{ ||\mathbf{A}\mathbf{x}||: \mathbf{x} \in \mathcal{R}^n ~~y~~||\mathbf{x}||=1 \right\}.$$
  \item Propiedades interesantes
    \begin{itemize}
      \item $||c\mathbf{A}||$ = $|c|~||\mathbf{A}||$
      \item $||\mathbf{A}_1 \pm \mathbf{A}_2|| \leq ||\mathbf{A}_1|| + ||\mathbf{A}_2||$
      \item $||\mathbf{A}\mathbf{x}|| \leq ||\mathbf{A}|| ~||\mathbf{x}||$
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Acotación y propagación de errores}
\begin{itemize}
  \item Suponemos un sistema determinado
  \item $$\mathbf{A}\mathbf{x} = \mathbf{y}.$$
  \item Queremos acotar el error en las soluciones $\delta\mathbf{x}$ a partir de los datos y los errores en las medidas $\delta\mathbf{A}$ y en las salidas del sistema $\delta\mathbf{y}$.
  \item Se obtiene
  \item $$\frac{||\delta\mathbf{x}||}{||\mathbf{x}||} \leq c_A \left[ \frac{||\delta\mathbf{y}||}{||\mathbf{y}||} + \frac{||\delta\mathbf{A}||}{||\mathbf{A}||}\right],$$
  \item donde $c_A \equiv ||\mathbf{A}||~||\mathbf{A}^{-1}||$.
  \item $c_A$ es el Número de Condición de la matriz $\mathbf{A}$.
\end{itemize}
\end{frame}

\begin{frame}{Sobre los espacios de dimensión infinita}
\begin{itemize}
  \item Ejemplos de espacios de dimensión infinita
    \begin{itemize}
      \item Los polinomios
      \item Las funciones continuas de $\mathcal{R} \longrightarrow \mathcal{R}$.
      \item Las funciones de $[0,1] \longrightarrow \mathcal{R}$, cuyo valor absoluto es integrable $L^1[0,1]$.
    \end{itemize}
  \item En casos concretos, como $L^1[0,1]$, se puede definir una norma.
  \item $$||f|| = \int_0^1 dx |f(x)|.$$
\end{itemize}
\end{frame}

\begin{frame}{Operadores lineales}
\begin{itemize}
  \item Los valores de una función vistos como coordenadas de un vector.
  \item Un operador lineal
  \item $$\mathcal{M}f(x) = \int dy ~m(x,y)f(y).$$
  \item Es un operador integral. Discutir.
\end{itemize}
\end{frame}

\begin{frame}{La integral indefinida}
\begin{itemize}
  \item La integral indefinida es un buen operador lineal sobre las funciones continuas
  \item $$\mathcal{G}(f)(x) = \int_0^x dy ~f(y).$$
  \item Discutir las propiedades del operador inversa por la izquierda (el operador derivada),
  \item $$\frac{d}{dx}.$$
  \item ¿Por qué solo inversa izquierda?
  \item ¿Recordamos métodos? Funciones de Green, convoluciones, etc.
\end{itemize}
\end{frame}

\begin{frame}{Números complejos}
\begin{itemize}
  \item Discusión: Hacer la lista de las propiedades interesantes.
  \item ¿Qué se puede agregar interesante en los espacios vectoriales complejos?
\end{itemize}
\end{frame}

\begin{frame}{Tarea}
\begin{itemize}
  \item Ejercicios 2.1.9 y 2.3.4
\end{itemize}
\end{frame}

\end{document}
