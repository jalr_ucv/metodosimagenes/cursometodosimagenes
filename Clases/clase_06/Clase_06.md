# Clase 6: Convolución
## Introducción

* Ejemplo: promedio móvil.

## Promedio móvil
* Definimos el promedio móvil de una función, sobre un intervalo $\delta$,

$$\frac{1}{\delta}\int_{x-\delta}^x dy~f(y).$$

* Para cada valor de $x$ el promedio se calcula con los valores de la función anteriores a $x$ en un intervalo de tamaño $\delta$.
* Muestra tendencias de la función para variaciones mayores que $\delta$.
* Elimina la contribución de componentes de frecuencias mayores a $\frac{1}{\delta}$.
* Podemos usar la función característica para definir $m(x)=\frac{1}{\delta}\chi_{[0,\delta]}(x)$. Entonces:

$$\mathcal{M}_{\delta}(f)=\int_{-\infty}^{\infty}dy~f(y)m_{\delta}(x-y).$$

## Transformada de Fourier de $\mathcal{M}_{\delta}(f)$
* Calculamos la transformada de Fourier de $\mathcal{M}_{\delta}(f)$:

$$\widehat{\mathcal{M}_{\delta}}(f)(\xi)=\hat{f}(\xi)\widehat{m_{\delta}}(\xi).$$

* Discutir.
* Generalizar a otras funciones $m_g(x)$
* Definición: La comvolución de las funciones $f$ y $g$ es la integral

$$f\star g(x) = \int_{-\infty}^{\infty}dy~f(x-y)g(y).$$

* Se cumple

$$\widehat{f\star g}(\xi)=f(\xi)g(\xi).$$

## Ejemplos de comvolución
* El promedio móvil aparece naturalmente cuando se censa una característica $f$ usando un instrumento de medida con resulución mas grande que $\delta$.

* Principio de superposición (3D)

$$\phi(\mathbf{x})=\int dy^3~\rho(\mathbf{y})\frac{1}{|\mathbf{x}-\mathbf{y}|}=\int dy^3~\rho(\mathbf{y})\phi_0(\mathbf{x}-\mathbf{y}).$$

* Funciones de Green

$$Df(x)=j(x),$$

$$DG(x)=\delta(x),$$

$$f(x)=G \star j(x).$$ 


## Definición mínima

* Si $f\in L^1$ y $g$ es localmente integrable y acotada, ambas funciones escalares en $\mathcal{R}^n$, la convolución de $f$ y $g$ se define así

$$f\star g(\mathbf{x}) = \int_{\mathcal{R}^n}dy^n~f(\mathbf{x}-\mathbf{y})g(\mathbf{y}).$$

* Propiedades

$$||f\star g||_{\infty}\leq||f||_{L^1}||g||_{\infty},$$

donde $||h||_{\infty}=sup\lbrace h(x),~x\in \mathcal{R}^n\rbrace$.

* La convolución es una función bien definida en general. (Detalles de convergencia omitidos en esta presentación)

## Propiedades
* Conmutatividad,

$$f\star g = g\star f.$$

* Asociatividad

$$(f\star g)\star h = f\star (g\star h)=f\star g\star h.$$

* Distributividad 

$$f\star (g + h) = f\star g + f\star h.$$

* Transformada de Fourier

$$\mathcal{F}(f\star g)(\xi)=\mathcal{F}(f)(\xi)~\mathcal{F}(g)(\xi).$$


## Filtros y filtros invariatnes de traslación

* Funcional: es una función de funciones. En ingeniería muchos funcionales se denominan filtros.

* La convolución es un funcional.

* La convolución queda dentro una clase de funcionales llamados filtros lineales. Definimos el filtro $C_\psi$

$$C_\psi(f)=\psi\star f.$$

* La convolución es un filtro invariante de traslaciones. Si definimos $f_\tau (x)=f(x-\tau)$, la función trasladada en $\tau$,

$$C_\psi(f_\tau)=(C_\psi(f))_\tau.$$

## Inversión de convolución
* Usando la transformada para recuperar $f$ a partir de $C_\psi(f)$,

$$f(\xi) = \frac{C_\psi(f)(\xi)}{\psi(\xi)},$$

$$f(x) = \mathcal{F}^{-1}(\frac{C_\psi(f)(\xi)}{\psi(\xi)})(x).$$

* ¿Qué problemas pueden surgir de esta operación?

## Regularidad de la convolución
* Sean $f$ localmente integrable y $g$ continua y de soporte acotado, entonces $f\star g$ es continua.  

* Si $g$ tiene $k$ derivadas continuas, $f\star g$ tiene $k$ derivadas continuas. Sea $j\leq k$,

$$\partial^j_x(f\star g) = f\star (\partial^j_x g).$$

* ¿Ejemplos conocidos?

## Aproximaciones de funciones

* Una convolución puede funcionar para generar una aproximación suave de una función.

* Sea $\varphi_{\epsilon}(x)$ una función diferenciable con soporte en $B_{\epsilon}$ y,

$$\int dx^n \varphi_{\epsilon}(x)=1.$$

* Si $f$ es una función razonable $f\star \varphi_{\epsilon}$ es una versión suvizada de $f$. 

* Si $\epsilon$ es pequeño $f\star \varphi_{\epsilon} \longrightarrow f$

## Soporte de la convolución

* Suma algebraica de conjuntos en $\mathcal{R}^n$

$$A+B=\lbrace x+y;~~x\in A,~ y\in B \rbrace.$$

* El soporte de $f\star g$ cumple, 

$$soporte(f\star g) \subseteq soporte(f) + soporte(g).$$

* Recordemos que el promedio, $f\star \varphi_{\epsilon}$, suaviza, pero también elimina detalles.

* Hacer más promedios elimina más detalles, pero hace la función más suave.

$$f\star \varphi_{\epsilon/j}\dots\star \varphi_{\epsilon/j},$$

hacemos la convolución $j$ veces.

* El soporte queda en $B_{\epsilon}$, pero la función es más suave (más promedios) que si $j=1$.

* Reducción de soporte, mas frecuencias altas incluídas.

* Más promedios, más suavidad.

* Es una forma de eliminar las frecuencias altas, pero suavizando la función de promedio.

## Función $\delta(x)$

* Es la identidad en convolución,

$$\delta\star f(x) = f(x).$$

* La transformada de Fourier

$$\hat\delta(\xi)=1.$$

* Aproximaciones de $\delta$ en espacio $x$

$$\varphi_{\epsilon}(x),$$

cuando $\epsilon \longrightarrow 0$.

Aproximaciones en espacio $\xi$,

$$\chi_{[-B,B]}(\xi),$$

cuando $B\longrightarrow \infty$.

La transformada inversa de $\chi_{[-B,B]}(\xi)$ es una función sinc. Si se usa una convolución de $\chi_{[-B,B]}\star\chi_{[-B,B]}(\xi)$, se obtiene una aproximación basada en sinc $^2$, que es absolutamente integrable y positiva.

## FWHM (Full-Width Half-Maximum)

* Definición de la resolución f\star \varphi$.

* $\varphi$ no negativa, con un único máximo en 0. Decrece de forma monótona hacia los lados del 0.

$$FWHM(\varphi)=x_2-x_1,$$

donde

$$\varphi(x_1)=\varphi(x_2)=\frac{\varphi(0)}{2}$$

## Tarea

* Ejercicios 5.1.15 y 5.3.4