\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}
\usepackage{pgfplots}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 6: Convolución}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{04 de septiembre de 2023} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Introducción}
\begin{itemize}
  \item Ejemplo: promedio móvil.
\end{itemize}
\end{frame}

\begin{frame}{Promedio móvil}
\begin{itemize}
  \item Definimos el promedio móvil de una función, sobre un intervalo $\delta$,
  $$\frac{1}{\delta}\int_{x-\delta}^x dy~f(y).$$
  \item Para cada valor de $x$ el promedio se calcula con los valores de la función anteriores a $x$ en un intervalo de tamaño $\delta$.
  \item Muestra tendencias de la función para variaciones mayores que $\delta$.
  \item Elimina la contribución de componentes de frecuencias mayores a $\frac{1}{\delta}$.
  \item Podemos usar la función característica para definir $m(x)=\frac{1}{\delta}\chi_{[0,\delta]}(x)$. Entonces:
  $$\mathcal{M}_{\delta}(f)=\int_{-\infty}^{\infty}dy~f(y)m_{\delta}(x-y).$$
\end{itemize}
\end{frame}

\begin{frame}{Transformada de Fourier de $\mathcal{M}_{\delta}(f)$}
\begin{itemize}
  \item Calculamos la transformada de Fourier de $\mathcal{M}_{\delta}(f)$:
  $$\widehat{\mathcal{M}_{\delta}}(f)(\xi)=\hat{f}(\xi)\widehat{m_{\delta}}(\xi).$$
  \item Discutir.
  \item Generalizar a otras funciones $m_g(x)$
  \item Definición: La convolución de las funciones $f$ y $g$ es la integral
  $$f\star g(x) = \int_{-\infty}^{\infty}dy~f(x-y)g(y).$$
  \item Se cumple
  $$\widehat{f\star g}(\xi)=f(\xi)g(\xi).$$
\end{itemize}
\end{frame}

\begin{frame}{Ejemplos de convolución}
\begin{itemize}
  \item El promedio móvil aparece naturalmente cuando se censa una característica $f$ usando un instrumento de medida con resolución más grande que $\delta$.
  \item Principio de superposición (3D)
  $$\phi(\mathbf{x})=\int dy^3~\rho(\mathbf{y})\frac{1}{|\mathbf{x}-\mathbf{y}|}=\int dy^3~\rho(\mathbf{y})\phi_0(\mathbf{x}-\mathbf{y}).$$
  \item Funciones de Green
  $$Df(x)=j(x),$$
  $$DG(x)=\delta(x),$$
  $$f(x)=G \star j(x).$$
\end{itemize}
\end{frame}

\begin{frame}{Definición mínima}
\begin{itemize}
  \item Si $f\in L^1$ y $g$ es localmente integrable y acotada, ambas funciones escalares en $\mathcal{R}^n$, la convolución de $f$ y $g$ se define así
  $$f\star g(\mathbf{x}) = \int_{\mathcal{R}^n}dy^n~f(\mathbf{x}-\mathbf{y})g(\mathbf{y}).$$
  \item Propiedades
  $$||f\star g||_{\infty}\leq||f||_{L^1}||g||_{\infty},$$
  donde $||h||_{\infty}=sup\lbrace h(x),~x\in \mathcal{R}^n\rbrace$.
  \item La convolución es una función bien definida en general. (Detalles de convergencia omitidos en esta presentación)
\end{itemize}
\end{frame}

\begin{frame}{Propiedades}
\begin{itemize}
  \item Conmutatividad,
  $$f\star g = g\star f.$$
  \item Asociatividad
  $$(f\star g)\star h = f\star (g\star h)=f\star g\star h.$$
  \item Distributividad 
  $$f\star (g + h) = f\star g + f\star h.$$
  \item Transformada de Fourier
  $$\mathcal{F}(f\star g)(\xi)=\mathcal{F}(f)(\xi)~\mathcal{F}(g)(\xi).$$
\end{itemize}
\end{frame}

\begin{frame}{Filtros y filtros invariantes de traslación}
\begin{itemize}
  \item Funcional: es una función de funciones. En ingeniería muchos funcionales se denominan filtros.
  \item La convolución es un funcional.
  \item La convolución queda dentro una clase de funcionales llamados filtros lineales. Definimos el filtro $C_\psi$
  $$C_\psi(f)=\psi\star f.$$
  \item La convolución es un filtro invariante de traslaciones. Si definimos $f_\tau (x)=f(x-\tau)$, la función trasladada en $\tau$,
  $$C_\psi(f_\tau)=(C_\psi(f))_\tau.$$
\end{itemize}
\end{frame}

\begin{frame}{Inversión de convolución}
\begin{itemize}
  \item Usando la transformada para recuperar $f$ a partir de $C_\psi(f)$,
  $$f(\xi) = \frac{C_\psi(f)(\xi)}{\psi(\xi)},$$
  $$f(x) = \mathcal{F}^{-1}\left(\frac{C_\psi(f)(\xi)}{\psi(\xi)}\right)(x).$$
  \item ¿Qué problemas pueden surgir de esta operación?
\end{itemize}
\end{frame}

\begin{frame}{Regularidad de la convolución}
\begin{itemize}
  \item Sean $f$ localmente integrable y $g$ continua y de soporte acotado, entonces $f\star g$ es continua.  
  \item Si $g$ tiene $k$ derivadas continuas, $f\star g$ tiene $k$ derivadas continuas. Sea $j\leq k$,
  $$\partial^j_x(f\star g) = (\partial^j_x f)\star g.$$
  \item ¿Ejemplos conocidos?
\end{itemize}
\end{frame}

\begin{frame}{Aproximaciones de funciones}
\begin{itemize}
  \item Una convolución puede funcionar para generar una aproximación suave de una función.
  \item Sea $\varphi_{\epsilon}(x)$ una función diferenciable con soporte en $B_{\epsilon}$ y,
  $$\int dx^n \varphi_{\epsilon}(x)=1.$$
  \item Si $f$ es una función razonable $f\star \varphi_{\epsilon}$ es una versión suavizada de $f$. 
  \item Si $\epsilon$ es pequeño $f\star \varphi_{\epsilon} \longrightarrow f$
\end{itemize}
\end{frame}

\begin{frame}{Soporte de la convolución}
\begin{itemize}
  \item Suma algebraica de conjuntos en $\mathcal{R}^n$
  $$A+B=\lbrace x+y;~~x\in A,~ y\in B \rbrace.$$
  \item El soporte de $f\star g$ cumple, 
  $$soporte(f\star g) \subseteq soporte(f) + soporte(g).$$
\end{itemize}
\end{frame}

\begin{frame}{Soporte de la convolución}
\begin{itemize}
  \item Recordemos que el promedio, $f\star \varphi_{\epsilon}$, suaviza, pero también elimina detalles.
  \item Hacer más promedios elimina más detalles, pero hace la función más suave.
  $$f\star \varphi_{\epsilon/j}\dots\star \varphi_{\epsilon/j},$$
  hacemos la convolución $j$ veces.
  \item El soporte queda en $B_{\epsilon}$, pero la función es más suave (más promedios) que si $j=1$.
  \item Reducción de soporte, más frecuencias altas incluidas.
  \item Más promedios, más suavidad.
  \item Es una forma de eliminar las frecuencias altas, pero suavizando la función de promedio.
\end{itemize}
\end{frame}

\begin{frame}{Función $\delta(x)$}
\begin{itemize}
  \item Es la identidad en convolución,
  $$\delta\star f(x) = f(x).$$
  \item La transformada de Fourier
  $$\hat\delta(\xi)=1.$$
  \item Aproximaciones de $\delta$ en espacio $x$
  $$\varphi_{\epsilon}(x),$$
  cuando $\epsilon \longrightarrow 0$.
\end{itemize}
\end{frame}

\begin{frame}{Función $\delta(x)$}
\begin{itemize}
  \item Aproximaciones en espacio $\xi$,
  $$\chi_{[-B,B]}(\xi),$$
  cuando $B\longrightarrow \infty$.
  \item La transformada inversa de $\chi_{[-B,B]}(\xi)$ es una función sinc. Si se usa una convolución de $\chi_{[-B,B]}\star\chi_{[-B,B]}(\xi)$, se obtiene una aproximación basada en sinc $^2$, que es absolutamente integrable y positiva.
\end{itemize}
\end{frame}

\begin{frame}{FWHM (Full-Width Half-Maximum)}
\begin{itemize}
  \item Definición de la resolución FWHM.
  \item $\varphi$ no negativa, con un único máximo en 0. Decrece de forma monótona hacia los lados del 0.
  $$FWHM(\varphi)=x_2-x_1,$$
  donde
  $$\varphi(x_1)=\varphi(x_2)=\frac{\varphi(0)}{2}$$
\end{itemize}
\end{frame}

\begin{frame}{Tarea}
\begin{itemize}
  \item Ejercicios 5.1.15 y 5.3.4
\end{itemize}
\end{frame}

\end{document}