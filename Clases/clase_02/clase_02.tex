\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 2: Medidas y modelado (infinitos grados de libertad)}

\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{10 de agosto de 2023} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Introducción}
\begin{itemize}
  \item En la clase anterior examinamos algunos sistemas físicos simples con un número finito de grados de libertad.
    \begin{itemize}
      \item Detectar la posición de un punto usando su sombra $(x,y)$.
      \item Medir la altura de una montaña usando un clinómetro $(h)$.
    \end{itemize}
  \item En los dos casos el estado del sistema se caracteriza por un número finito (y reducido) de parámetros.
  \item El problema de determinar el estado del sistema a partir de mediciones factibles se reduce a resolver sistemas de un número finito de ecuaciones con un número finito de incógnitas.
  \item En las aplicaciones de imagen, el estado de un sistema suele ser descrito por una función o funciones de variables continuas. Estos sistemas tienen un número infinito de grados de libertad.
\end{itemize}
\end{frame}

\begin{frame}{Ejemplo: Determinar la forma de la frontera de una región en el plano}
\begin{columns}[t] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\column{.5\textwidth} % Left column and width

\begin{itemize}
  \item El estado del sistema está determinado por la forma de una curva cerrada en el plano.
  \item En principio se puede describir con una función. Para conocerla se deben conocer el dominio, el rango y todas las (infinitas) relaciones entre ellos.
\end{itemize}

\column{.5\textwidth} % Right column and width

\center
\begin{tikzpicture}[use Hobby shortcut]
  \path
  (0,0) coordinate (z0)
  (1.2,.8) coordinate (z1)
  (.8,1.8) coordinate (z2)
  (.2,1.4) coordinate (z3)
  (.6,1) coordinate (z4);
  \draw[closed] (z0) .. (z1) .. (z2) .. (z3) .. (z4);
\end{tikzpicture}

\end{columns}
\end{frame}

\begin{frame}{Ejemplo: Determinar la forma de una región plana convexa}
\begin{columns}[t] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\column{.5\textwidth} % Left column and width
\begin{itemize}
  \item Región convexa
    \begin{itemize}
      \item Cualquier par de puntos dentro de la región definen un segmento recto que también está dentro de la región.
      \item No hay penínsulas, cabos o dedos.
    \end{itemize}
\end{itemize}

\column{.5\textwidth} % Right column and width

\center
\begin{tikzpicture}[use Hobby shortcut]
  \path
  (0,0) coordinate (z0)
  (1.2,.8) coordinate (z1)
  (.8,1.8) coordinate (z2)
  (.1,1.4) coordinate (z3)
  (-.1,1) coordinate (z4);
  \draw[closed] (z0) .. (z1) .. (z2) .. (z3) .. (z4);
\end{tikzpicture}

\end{columns}

\end{frame}

\begin{frame}{Ejemplo: Determinar la forma de una región plana convexa}
\begin{columns}[t] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\column{.5\textwidth} % Left column and width
\begin{itemize}
  \item Modelo de imagen
    \begin{itemize}
      \item Determinación de la forma = Determinación de la imagen.
      \item Podemos enviar partículas que reboten contra la superficie desconocida y registrar su dirección de salida.
      \item Necesitamos un modelo físico de reflexión: ángulo de impacto = ángulo de rebote.
    \end{itemize}
\end{itemize}

\column{.5\textwidth} % Right column and width

\center
\begin{tikzpicture}[use Hobby shortcut]
  \path
  (0,0) coordinate (z0)
  (1.2,.8) coordinate (z1)
  (.8,1.8) coordinate (z2)
  (.1,1.4) coordinate (z3)
  (-.1,1) coordinate (z4);
  \draw[closed] (z0) .. (z1) .. (z2) .. (z3) .. (z4);
  \draw[->] (-.2,3) -- (.5, 2);
  \draw[->] (.65,2) -- (1.2, 3.2);
\end{tikzpicture}

\end{columns}

\end{frame}

\begin{frame}{Ejemplo: Determinar la forma de un objeto plano mediante sus sombras}
\begin{itemize}
  \item Espacio de líneas en el plano
    \begin{itemize}
      \item Geometría plana y formas de representar una recta
        \begin{itemize}
          \item Función en el plano. $f:\mathbb{R} \rightarrow \mathbb{R}$, $y=mx + b$.
          \item Función paramétrica. $\vec{r}:\mathbb{R} \rightarrow \mathbb{R}^2$, $\vec{r}(t)=\mathbf{r}_0 + \mathbf{V}t$. ($\mathbf{V}\neq \mathbf{0}$).
          \item Ecuación canónica. $F(x,y)=ctte$, $ax+by=c$. ($a^2+b^2\neq 0$).
        \end{itemize}
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Ecuación canónica y líneas orientadas}
\begin{itemize}
  \item La ecuación canónica en dibujos
    \begin{itemize}
      \item $ax+by=c$
      \item $(a,b)$ es un vector (director) perpendicular a la recta.
      \item $|c|$ es la distancia de la recta al origen de coordenadas.
    \end{itemize}
  \item Se puede normalizar toda la ecuación dividiendo entre la norma del vector director, $\mathbf{\omega}=(\omega_1,\omega_2)=(a,b)/(\sqrt{a^2+b^2})$, $t=c/(\sqrt{a^2+b^2})$ se sigue obteniendo la misma recta.
  \item $\langle \mathbf{\omega},\mathbf{r}\rangle=t$
  \item Los vectores directores normalizados viven sobre un círculo.
\end{itemize}
\end{frame}

\begin{frame}{Determinar la forma de un objeto usando sombras proyectadas}

\begin{columns}[t] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment
\column{.5\textwidth} % Left column and width

\begin{itemize}
  \item Propongamos una relación matemática entre la dirección de una fuente luminosa con frente plano y la sombra proyectada por un objeto en 2 dimensiones.
\end{itemize}

\column{.5\textwidth} % Right column and width

\center
\begin{tikzpicture}[use Hobby shortcut]
  \path
  (0.5,0.5) coordinate (z0)
  (1.7,1.3) coordinate (z1)
  (1.3,2.3) coordinate (z2)
  (.6,1.9) coordinate (z3)
  (.4,1.5) coordinate (z4);
  \draw[closed] (z0) .. (z1) .. (z2) .. (z3) .. (z4);
  \draw[->] (-1,0) -- (2, 0);
  \draw[->] (0,-1) -- (0, 3);
  
  \draw[-] (-1,-.5) -- (2.4, 1.2);
  
  \draw[->]  (-1.2, 2.4) -- (-.25,.5);
  \draw[->]  (-0.8, 2.6) -- (.15,.7);
  \draw[->]  (-0.4, 2.8) -- (.55,.9);
  \draw[->]  (0.0, 3.0) -- (0.95,1.1);
  \draw[->]  (0.4, 3.2) -- (1.35,1.3);
  \draw[->]  (0.8, 3.4) -- (1.75,1.5);
  \draw[->]  (1.2, 3.6) -- (2.15,1.7);
\end{tikzpicture}

\end{columns}

\end{frame}

\begin{frame}{Matemática de la sombra}
\begin{itemize}
  \item Definimos la función sombra $h(\theta)$ 
  \item Escogemos de forma astuta una de las proyecciones extremas (los bordes de la sombra)
  \item La línea tangente a la superficie tiene esta parametrización $\mathbf{r}(\theta,s)=h(\theta)\mathbf{\omega}_{\theta} + s\mathbf{\hat\omega}_{\theta}$
\end{itemize}
\end{frame}

\begin{frame}{Matemática de la sombra}
\begin{itemize}
  \item Para cada proyección ($\theta$) hay un valor de $s$ tal que el punto $\mathbf{r}(\theta)$ está sobre la superficie. 
  \item Hay una función $s(\theta)$ que guarda esa información $\mathbf{r}(\theta)=h(\theta)\mathbf{\omega}_{\theta} + s(\theta)\mathbf{\hat\omega}_{\theta}$.
  \item La derivada $\dot{\mathbf{r}}(\theta)$ es tangente a la superficie y por lo tanto es paralela a $\mathbf{\hat\omega}_{\theta}$. Entonces $\dot{h}=s$.
  \item $\mathbf{r}(\theta)=h\mathbf{\omega} + \dot{h}\mathbf{\hat\omega}$.
\end{itemize}
\end{frame}

\begin{frame}{Discusión}
\begin{itemize}
  \item ¿Cómo podemos usar la relación $\mathbf{r}(\theta)=h\mathbf{\omega} + \dot{h}\mathbf{\hat\omega}$ para reconstruir la frontera de la región en términos prácticos?
  \item ¿Hay otras fomas de plantear el problema de reconstrucción de la imagen?
 \end{itemize}
\end{frame}

\begin{frame}{Tarea 02}
Ejercicios 1.1.15, 1.1.16, 1.1.18, 1.2.1, 1.2.2 y 1.2.7, 1.2.8 y 1.2.13
\end{frame}

\end{document}
