# Clase 2: Medidas y modelado (infinitos grados de libertad)
## Introducción

* En la clase anterior, examinamos algunos sistemas físicos simples con un número finito de grados de libertad.
    * Detectar la posición de un punto usando su sombra $(x,y)$.
    * Medir la altura de una montaña usando un clinómetro $(h)$.
* En los dos casos el estado del sistema se caracteriza por un número finito (y reducido) de parámtros.
* El problema de determinar el estado del sistema a partir de mediciones factibles se reduce a resolver sistemas de un número finito de ecuaciones con un número finito de incógnitas.
* En las aplicaciones de imagen, el estado de un sistema suele ser descrito por una función o funciones de variables continuas. Estos sistemas tienen un número infinito de grados de libertad.

### Ejemplo: Determinar la forma de la frontera de una región en el plano.
* El estado del sistema está determinado por la forma de una curva cerrada en el plano.
* En principio se puede describir con una función. Para conocerla se deben conocer el dominio, el rango y todas las (infinitas) relaciones entre ellos.

### Ejemplo: Determinar la forma de una región plana convexa que no es inaccesible macroscópicamente.

#### Región convexa

* Cualquier par de puntos dentro de la región definen un segmento recto que también está dentro de la región.
* No hay penínsulas, cabos o dedos.
* Discusión:
    * El polígono con vértices en 3 o más puntos en la frontera de una superfície convexa está totalmente incluído en la superficie.
    * El polígono formado al intersectar los semiplanos defindos por las rectas tangentes a 3 o más puntos de la frontera de la superficie contiene a toda la superficie.

#### Modelo de imagen

* Determinación de la forma = Determinación de la imagen.
* Podemos enviar partículas que reboten contra la superficie desconocida y registrar su dirección de salida.
* Necesitamos un modelo físico de reflexión: ángulo de impacto = ángulo de rebote.

* Discusión:
    * ¿Cómo hayar los puntos de la frontera con la información de entrada y salida de partículas?
    * Hace falta enviar un número infinito de partículas para considerar todos los rebotes posibles.
    * Usando las propiedades de convexidad, podemos proponer aproximaciones poligonales a la imagen buscada.
    * La determinación de la forma de la frontera del objeto se convierte en un problema de finitos grados de libertad.
    * Siempre ocurre así en casos prácticos.
    * Recordar:
        * Estado del sistema = vector = arreglo de valores.
        * Sistemas pueden tener fintos o infinitos grados de libertad = dimensión de un espacio vectorial
        * La posición de un punto en el espacio es un sistema de finitos grados de libertad.
        * La forma de un objeto (imagen) es un sistema de infinitos grados de libertad.
        * La información del estado del sistema puede obtenerse a partir de la solición de un sistema de ecuaciónes lineales o no lineales.
        * Estas ecuaciónes pueden obtenerse a partir del conocimiento de interacciones físicas con el sistema a estudiar.
        * En casos prácticos los sistemas de infinitos grados de libertad se aproximan con sistemas de finitos grados de libertad.

### Determinar la imagen de un objeto plano mediante el conocimiento de sus sombras

* Consideramos la posibilidad de iluminar un objeto opaco.
* No veremos la luz reflejada, pero podríamos observar la sombra que proyecta en un plano.
* ¿Cómo podemos extraer información sobre su imagen?

#### Espacio de lineas en el plano

* Geometría plana y formas de representar una recta
    * Función en el plano. $f:R \rightarrow R$, $y=mx + b$.
    * Función paramétrica. $s:R \rightarrow R^2$, $\vec{r}(t)=\mathbf{r}_0 + \mathbf{V}t$. ($\mathbf{V}\neq \mathbf{0}$).
    * Ecuación canónica. $F(x,y)=ctte$, $ax+by=c$. ($a^2+b^2\neq 0$).

#### Ecuación canónica y lineas orientadas

* La ecuación canónica en dibujos
    * $ax+by=c$
    * $(a,b)$ es un vector (director) perpendicular a la recta.
    * $|c|$ es la distancia de la recta al origen de coordenadas.
    * Obvio: el cambio de signo de las tres constantes define un vector director distinto, pero es la misma recta.
* Se puede normalizar toda la ecuación dividendo entre la norma del vector director, 
$\mathbf{\omega}=(\omega_1,\omega_2)=(a,b)/(\sqrt{a^2+b^2})$, $t=c/(\sqrt{a^2+b^2})$ se sigue obteniendo la misma recta.
    * $\langle \mathbf{\omega},\mathbf{r}\rangle=t$
* Los vectores directores normalizados viven sobre un círculo.
* La ambigüedad en el cambio de signo del vector director y la constante $t$ se resuelve agregando una dirección a la recta.
    * Definimos el vector $\mathbf{\hat\omega}\equiv (-\omega_2,\omega_1)$ para indicar la dirección de la recta asociada al par ($\mathbf\omega,t$).
#### Determinar la forma de un objeto usando las diferentes sombras proyectadas

* Propongamos una relación matemática entre la dirección de una fuente luminosa con frente plano y la sombra proyectada por un objeto en 2 dimensiones.

* Dibujo

* Para discutir:
    * Describa las propiedades de la sombra del objeto.
    * ¿Qué información relativa a la superficie podemos extraer de la sombra?

* Matemática de la sombra
    * Definimos la función sombra $h(\theta)$ 
    * Escogemos de forma astuta una de las proyecciones extremas (los bordes de la sombra)
* Para discutir:
    * La linea tangente a la superficie tiene esta parametrización $\mathbf{r}(\theta,s)=h(\theta)\mathbf{\omega}_{\theta} + s\mathbf{\hat\omega}_{\theta}$
    * Para cada proyección ($\theta$) hay un valor de $s$ tal que el punto $\mathbf{r}(\theta)$ está sobre la superficie. 
    * Hay una función $s(\theta)$ que guarda esa información $\mathbf{r}(\theta)=h(\theta)\mathbf{\omega}_{\theta} + s(\theta)\mathbf{\hat\omega}_{\theta}$.
    * La derivada $\dot{\mathbf{r}}(\theta)$ es tangente a la superficie y por lo tanto es paralela a $\mathbf{\hat\omega}_{\theta}$. Entonces $\dot{h}=s$.
    * $\mathbf{r}(\theta)=h\mathbf{\omega} + \dot{h}\mathbf{\hat\omega}$.

### Tarea
Ejercicios 1.1.15, 1.1.16, 1.1.18, 1.2.1, 1.2.2 y 1.2.7, 1.2.8 y 1.2.13
