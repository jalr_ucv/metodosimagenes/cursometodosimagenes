# Clase 11: Filtros
## Introducción

* Filtro: es un mapa entre dos espacios de funciones.
    * Es casi cualquier funcional.
    * Hemos trabajado con filtros todo este curso.
* Importante: 
    * Filtros lineales.
    * Invariancia de traslación (Shift invariant)
* Esta clase estudia varias transformaciones vistas como filtros.

## Definiciones básicas
* Suponemos una función real $x(t)$ la acción del filtro se indica con una transformación $\mathcal{A}$.
* Existen filtros en funciones de varias variables (obvio). En general es suficiente estudiar en funciones de una variable.

## Algunos ejemplos elementales

* Escalamiento. Sea $(a>0)$:

$$\mathcal{A}x(t) = ax(t).$$

* Traslación. Sea $\tau$ una constante:

$$\mathcal{A}_{\tau}x(t) = x(t-\tau).$$

* Producto por una función $\psi(t)$:

$$\mathcal{M}_{\psi}x(t) = \psi(t)x(t).$$


## Más ejemplos elementales

* Convolución:

$$\mathcal{C}_{\varphi}x(t) = \varphi\star x(t) = \int_{-\infty}^{\infty}\varphi(t-s)x(s).$$

* Derivada:

$$\mathcal{D}x(t) = \frac{d}{dt} x(t).$$

* Transformada de Fourier:

$$\mathcal{F}: x(t) \longrightarrow \hat x(\xi).$$

## Filtros lineales

* Todos los ejemplos anteriores son filtros lineales

$$\mathcal{A}(ax_a(t) + bx_b(t)) = a\mathcal{A}x_a(t)+b\mathcal{A}x_b(t).$$

* Se mantiene la combinación lineal de funciones. Comentar producto por una función y convolución.

* Proponer un filtro no lineal.

* Un diodo opera sobre el voltaje aplicado, entregando un voltaje de salida. Discutir:

$$\mathcal{R}x(t) = \chi_{\left[ V_f,\infty \right)}(x(t))x(t).$$

## Proceso de medición

* Un proceso de medición tiene un filtro asociado.
    * Ejemplo común: promedio pesado $(\varphi(t)>0)$

$$\mathcal{A}_{\varphi}x(t) = \varphi\star x(t) = \int_{-\infty}^{\infty}\varphi(t-s)x(s).$$

* Es una convolución (tal como ya habíamos comentado)

* La convolución es lineal, pero algunos procesos instrumentales saturan a ciertos valores de señal $T$

$$G(x)=\lbrace\begin{array}{lr} x;&~~|x|<T \\
T; &~~x>T \\
-T; &~~x<-t\\
\end{array} \rbrace$$

* El proceso de medida (no lineal) está modelado:

$$\mathcal{A}_{\varphi}x(t) = \varphi\star x(t) = \int_{-\infty}^{\infty}\varphi(t-s)G[x(s)].$$

## Más sobre filtros lineales

* La forma general de un filtro lineal es una integral

$$\mathcal{A}x(t) = \int_{-\infty}^{\infty}a(t,s)x(s).$$

* $a(t,s)$ es el kernel.

* Kernel antiderivada,

$$a(t,s)=H(t-s).$$

* Kernel derivada,

$$a(t,s)=\delta'(t-s).$$

## Filtros invariantes de traslación

* Operación de traslación,

$$x_{\tau}(t)=x(t-\tau).$$

* Un filtro invariante de traslaciones cumple:

$$\mathcal{A}(x_{\tau}) = (\mathcal{A}x_{\tau}).$$


## Filtros invariantes de traslaciones y convolución

* Todo filtro lineal, invariante de traslaciones, es una convolución,

$$a(t,s)=a(t-s,0).$$

* Discutir.

## Función de transferencia (Descripción en espacio de frecuencia)

* En un filtro definido por una convolución, la transformada de Fourier del kernel se denomina función de transferencia (transfer function)

$$\widehat{k \star x}(\xi) = \hat k(\xi)\hat x(\xi).$$

* La función de transferencia se separa trivialmente en amplitud y fase

$$\hat k(\xi) = A(\xi)e^{i\theta(\xi)}.$$

* Amplitud $A(\xi)$.

* Fase $\theta(\xi)$.

* Un filtro lineal, invariante de traslaciones, en espacio de frecuencia:

$$\mathcal{A}x(t) = \mathcal{F}^{-1}(\hat k \hat x)(t) = \frac{1}{2\pi}\int_{-\infty}^{\infty}\hat k(\xi)\hat x(\xi)e^{ix\xi}d\xi.$$

## Filtros en cascada

* El filtrado en cascada es equivalente a la composición de transformaciones

$$x \longrightarrow \mathcal{A}_1x\longrightarrow \mathcal{A}_2(\mathcal{A}_1x)\longrightarrow \dots = \mathcal{A}_n\circ \mathcal{A}_{n-1}\circ \dots\circ\mathcal{A}_1~x$$

* Incluso en las operaciones lineales $\left[A,B\right]\neq 0$.

* Los filtros de convolución sí conmutan.

* Es conveniente el análisis en el espacio de frecuencia (funciones de transferencia)

$$\hat k_{total} = \hat k_1\dots \hat k_n$$

* Discutir.

## Ejercicios

* Ejercicio 9.1.14
* Reproducir y discutir el ejemplo 9.1.26
