\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usetikzlibrary{hobby}
\usepackage{pgfplots}

\title{Métodos matemáticos para el procesamiento de imágenes médicas}
\subtitle{Clase 11: Filtros}


\author{José Antonio López} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{19 de octubre de 2023} % Date, can be changed to a custom date

\begin{document}

\frame{\titlepage}

\begin{frame}{Introducción}
    \begin{itemize}
        \item Filtro: es un mapa entre dos espacios de funciones.
        \begin{itemize}
            \item Es casi cualquier funcional.
            \item Hemos trabajado con filtros todo este curso.
        \end{itemize}
        \item Importante:
        \begin{itemize}
            \item Filtros lineales.
            \item Invariancia de traslación (Shift invariant).
        \end{itemize}
        \item Esta clase estudia varias transformaciones vistas como filtros.
    \end{itemize}
\end{frame}

\begin{frame}{Definiciones básicas}
    \begin{itemize}
        \item Suponemos una función real $x(t)$. La acción del filtro se indica con una transformación $\mathcal{A}$.
        \item Existen filtros en funciones de varias variables (obvio). En general es suficiente estudiar en funciones de una variable.
    \end{itemize}
\end{frame}

\begin{frame}{Algunos ejemplos elementales}
    \begin{itemize}
        \item Escalamiento. Sea $(a>0)$:
        $$\mathcal{A}x(t) = ax(t).$$
        \item Traslación. Sea $\tau$ una constante:
        $$\mathcal{A}_{\tau}x(t) = x(t-\tau).$$
        \item Producto por una función $\psi(t)$:
        $$\mathcal{M}_{\psi}x(t) = \psi(t)x(t).$$
    \end{itemize}
\end{frame}

\begin{frame}{Más ejemplos elementales}
    \begin{itemize}
        \item Convolución:
        $$\mathcal{C}_{\varphi}x(t) = \varphi\star x(t) = \int_{-\infty}^{\infty}\varphi(t-s)x(s).$$
        \item Derivada:
        $$\mathcal{D}x(t) = \frac{d}{dt} x(t).$$
        \item Transformada de Fourier:
        $$\mathcal{F}: x(t) \longrightarrow \hat x(\xi).$$
    \end{itemize}
\end{frame}

\begin{frame}{Filtros lineales}
    \begin{itemize}
        \item Todos los ejemplos anteriores son filtros lineales.
        $$\mathcal{A}(ax_a(t) + bx_b(t)) = a\mathcal{A}x_a(t)+b\mathcal{A}x_b(t).$$
        \item Se mantiene la combinación lineal de funciones. Comentar producto por una función y convolución.
        \item Proponer un filtro no lineal.
        \item Un diodo opera sobre el voltaje aplicado, entregando un voltaje de salida. Discutir:
        $$\mathcal{R}x(t) = \chi_{\left[0,\infty \right)}(x(t))x(t).$$
    \end{itemize}
\end{frame}

\begin{frame}{Proceso de medición}
    \begin{itemize}
        \item Un proceso de medición tiene un filtro asociado.
        \begin{itemize}
            \item Ejemplo común: promedio pesado $(\varphi(t)>0)$
            $$\mathcal{A}_{\varphi}x(t) = \varphi\star x(t) = \int_{-\infty}^{\infty}\varphi(t-s)x(s).$$
            \item Es una convolución (tal como ya habíamos comentado).
            \item La convolución es lineal, pero algunos procesos instrumentales saturan a ciertos valores de señal $T$
            $$G(x)=\left\{ \begin{array}{lr} x;&~~|x|<T \\
            T; &~~x>T \\
            -T; &~~x<-T\\
            \end{array} \right.$$
            \item El proceso de medida (no lineal) está modelado:
            $$\mathcal{A}_{G\varphi}x(t) = \varphi\star G[x](t) = \int_{-\infty}^{\infty}\varphi(t-s)G[x(s)].$$
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Más sobre filtros lineales}
    \begin{itemize}
        \item La forma general de un filtro lineal es una integral
        $$\mathcal{A}x(t) = \int_{-\infty}^{\infty}a(t,s)x(s).$$
        \item $a(t,s)$ es el kernel.
        \item Kernel antiderivada,
        $$a(t,s)=H(t-s).$$
        \item Kernel derivada,
        $$a(t,s)=\delta'(t-s).$$
    \end{itemize}
\end{frame}

\begin{frame}{Filtros invariantes de traslación}
    \begin{itemize}
        \item Operación de traslación,
        $$x_{\tau}(t)=x(t-\tau).$$
        \item Un filtro invariante de traslaciones cumple:
        $$\mathcal{A}(x_{\tau}) = (\mathcal{A}x)_{\tau}.$$
    \end{itemize}
\end{frame}

\begin{frame}{Filtros invariantes de traslaciones y convolución}
    \begin{itemize}
        \item Todo filtro lineal, invariante de traslaciones, es una convolución,
        $$a(t,s)=a(t-s,0).$$
        \item Discutir.
    \end{itemize}
\end{frame}

\begin{frame}{Función de transferencia (Descripción en espacio de frecuencia)}
    \begin{itemize}
        \item En un filtro definido por una convolución, la transformada de Fourier del kernel se denomina función de transferencia (transfer function)
        $$\widehat{k \star x}(\xi) = \hat k(\xi)\hat x(\xi).$$
        \item La función de transferencia se separa trivialmente en amplitud y fase
        $$\hat k(\xi) = A(\xi)e^{i\theta(\xi)}.$$
        \item Amplitud $A(\xi)$.
        \item Fase $\theta(\xi)$.
        \item Un filtro lineal, invariante de traslaciones, en espacio de frecuencia:
        $$\mathcal{A}x(t) = \mathcal{F}^{-1}(\hat k \hat x)(t) = \frac{1}{2\pi}\int_{-\infty}^{\infty}\hat k(\xi)\hat x(\xi)e^{ix\xi}d\xi.$$
    \end{itemize}
\end{frame}

\begin{frame}{Filtros en cascada}
    \begin{itemize}
        \item El filtrado en cascada es equivalente a la composición de transformaciones
        $$x \longrightarrow \mathcal{A}_1x\longrightarrow \mathcal{A}_2(\mathcal{A}_1x)\longrightarrow \dots = \mathcal{A}_n\circ \mathcal{A}_{n-1}\circ \dots\circ\mathcal{A}_1~x$$
        \item Incluso en las operaciones lineales $\left[A,B\right]\neq 0$.
        \item Los filtros de convolución sí conmutan.
        \item Es conveniente el análisis en el espacio de frecuencia (funciones de transferencia)
        $$\hat k_{total} = \hat k_1\dots \hat k_n$$
        \item Discutir.
    \end{itemize}
\end{frame}

\begin{frame}{Ejercicios}
    \begin{itemize}
        \item Ejercicio 9.1.14
        \item Reproducir y discutir el ejemplo 9.1.26
    \end{itemize}
\end{frame}

\end{document}